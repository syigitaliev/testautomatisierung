package de.bring.pl_mobil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Base64;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.junit.Rule;
import org.junit.rules.ExternalResource;

public class TestBase {
	protected WebDriver driver;
    private static XSSFCell cell;
	
	// Global test data excel file
	public static final String testDataExcelFileName= "testdata.xlsx";
	
	@Rule
	public ExternalResource externalResource = new ExternalResource() {
		@Override
		protected void before() throws Throwable {
			System.setProperty("webdriver.chrome.driver",
					System.getProperty("user.dir") + "\\src\\test\\java\\resources\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().window().maximize();
		}

		@Override
		protected void after() {
			//driver.close();
			// driver.quit();
		}
	};

	
	public static String decrypt(String encryptedPassword) {
		String decryptedPassword;
		byte[] decryptedPasswordBytes = Base64.getDecoder().decode(encryptedPassword);
		decryptedPassword = new String(decryptedPasswordBytes);
		return decryptedPassword;
		}
	public static String encrypt(String password) {
		String encryptedPassword;
		byte[] encodedBytes = Base64.getEncoder().encode(password.getBytes());
		encryptedPassword = new String(encodedBytes);
		return encryptedPassword;
		}
	
	
	
	public void updateProperty(String config, String value) throws ConfigurationException {

		PropertiesConfiguration conf;
		
		conf = new PropertiesConfiguration(
				System.getProperty("user.dir") + "\\src\\main\\resources\\application.properties");
		conf.setProperty(config, value);
		conf.save();

	}
	
	public String readProperty(String str) {
		String prop;
		PropertiesConfiguration conf;

		try {
			conf = new PropertiesConfiguration(
					System.getProperty("user.dir") + "\\src\\main\\resources\\application.properties");
			prop = conf.getProperty(str).toString();
			conf.save();
			return prop;
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "leer";
		}
	}

	
	
	public static Object[][] getExcelData(String ExcelFile, String sheetName) throws IOException {
		String[][] arrayExcelData = null;
		try {
			File src = new File(ExcelFile);
	        FileInputStream file = new FileInputStream(src); 
	        
	      //  FileInputStream file = new FileInputStream(new File(ExcelFile));
	        XSSFWorkbook excelWBook = new XSSFWorkbook(file);
	        XSSFSheet excelWSheet = excelWBook.getSheet(sheetName);
	        DataFormatter formatter = new DataFormatter();
		     
           
            int totalNoOfRows = excelWSheet.getLastRowNum();
            
            System.out.println("Anzahl Zeile: " +totalNoOfRows );
            
            int totalNoOfCols =excelWSheet.getRow(0).getLastCellNum();
            
            System.out.println("Anzahl Spalten: " +totalNoOfCols);
			
            // Inizialisiere ein Array für die Daten
			arrayExcelData = new String[totalNoOfRows][totalNoOfCols];
			
			// Speichere die Daten in den Array
			for (int RowNum= 1 ; RowNum <= totalNoOfRows; RowNum++) {

				for (int ColNum=0; ColNum < totalNoOfCols; ColNum++) {
					
					  cell = excelWSheet.getRow(RowNum).getCell(ColNum);
			            //DataFormatter formatter = new DataFormatter();
			            String cellData = formatter.formatCellValue(cell);
			           // return cellData;
					arrayExcelData[RowNum-1][ColNum] = cellData;
					 System.out.println("Die Zeile "+RowNum+" und die Spalte "+(ColNum+1)+ " ist "+cellData);
				}

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
			e.printStackTrace();
		}
		return arrayExcelData;
	}
	
	public static String  readDataFromExcel(String ExcelFile, String sheetName, String value) throws IOException {
		int RowNum=1, ColNum=1;    String  cellData = null;
			try {
				File src = new File(ExcelFile);
		        FileInputStream file = new FileInputStream(src); 
		        
		      //  FileInputStream file = new FileInputStream(new File(ExcelFile));
		        XSSFWorkbook excelWBook = new XSSFWorkbook(file);
		        XSSFSheet excelWSheet = excelWBook.getSheet(sheetName);
		        DataFormatter formatter = new DataFormatter();
			     
	           
	            int totalNoOfRows = excelWSheet.getLastRowNum();
	            
	            System.out.println("Anzahl Zeile: " +totalNoOfRows );
	            
	            int totalNoOfCols =excelWSheet.getRow(0).getLastCellNum();
	            
	            System.out.println("Anzahl Spalten: " +totalNoOfCols);
				
	            // Inizialisiere ein Array für die Daten
				// arrayExcelData = new String[totalNoOfRows][totalNoOfCols];
				
				// Speichere die Daten in den Array
			   // Identifizierung der  
	            if(value== "Standort") {
			    	  RowNum=1; ColNum=1;
					}
	            
	            else if(value== "Einloggen") {
			    	  RowNum=2; ColNum=1;
					}
			  	else if (value== "Auswahl_Kommissionierung" || value== "Auswahl_Konsolidierung") {
			  	  RowNum=3; ColNum=1;
					}
			  	else if (value== "Scanne_Komm_Gestell") {
			    	  RowNum=4; ColNum=1;
			  		}
			  	else if (value== "Starte_Kommissionierung" ||  value== "Starte_Konsolidierung") {
			  	  RowNum=5; ColNum=1;
					}
			  	else if (value== "Scanne_ET_Drucker" || value== "Abschluss") {
			    	  RowNum=6; ColNum=1;
			  		}
			  	else if (value== "Starte_direkt_ Konsolidierung") {
			  	  RowNum=7; ColNum=1;
					}
			  	else System.out.println("Prozess ist in dem Tabelle "+sheetName+ "nicht definiert." );
			            				
			      // Lese den Wert von der Zelle 
						  cell = excelWSheet.getRow(RowNum).getCell(ColNum);
				            //DataFormatter formatter = new DataFormatter();
				              cellData = formatter.formatCellValue(cell);
				           // return cellData;
						
						 System.out.println("Die Zeile "+RowNum+" und die Spalte "+(ColNum)+ " ist "+cellData);
					
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
				e.printStackTrace();
			}
			return cellData;
		}
	
	
}

