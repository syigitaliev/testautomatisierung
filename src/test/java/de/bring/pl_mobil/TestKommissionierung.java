package de.bring.pl_mobil;


import org.apache.commons.configuration.ConfigurationException;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import pageObjects.Kommissionierung;
import pageObjects.Konsolidierung;
import pageObjects.Login;
import pageObjects.MainPage;

 @RunWith(DataProviderRunner.class)
public class TestKommissionierung extends TestBase {
	private Login login; 
	private MainPage mainPage;
	
	private Kommissionierung kommissionierung;
	private ConnectToDB dbCon;
	private Konsolidierung kon;
    public static HSSFWorkbook workbook;
    public static HSSFSheet worksheet;
    public static DataFormatter formatter= new DataFormatter();
    public static String file_location = System.getProperty("user.dir")+"/src";
    static String SheetName= "Tabelle1";
   
 
	@Before
	public void SetUp() throws Exception {

		login = new Login(driver);
		mainPage = new MainPage(driver);
		kommissionierung = new Kommissionierung(driver);
		dbCon = new ConnectToDB();
		kon = new Konsolidierung(driver);
	}
	
	
	@Test
    @UseDataProvider("loginData")
	public void kommissioniere(String mde, String user, String password) throws Throwable {
		// Aufruf der mobilen Anwendung von Berlin
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Kommissionierung","Standort (BER oder MUC)" ).contains("BER")) {
		String uRL ="http://172.31.0.42:62670/plbring/servlet/start?mde="+mde;
		login.visit(uRL);
		}
		// Aufruf der mobilen Anwendung von München
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Kommissionierung","Standort (BER oder MUC)" ).contains("MUC")) {
		//String uRL_MUC = "http://172.31.0.58:62670/plbring/servlet/start?mde="+mde;
		String uRL_MUC = "http://wms-app-muenchen.abholmeister.de:62670/pllvs681/servlet/start?mde="+mde;
		login.visit(uRL_MUC);
		}
		// die Methode readDataFromExcel dient zur Bestimmung des Prozessablaufs im Vorfeld 
		// Beinhaltet der Prozess von der Tabelle "Kommissionierung" den Wert 1, dann werden die entsprechenden Methoden zu dem Prozess ausgeführt.
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Kommissionierung","Einloggen" ).contains("1")) {
			
			login.typeUsername(user);
			login.typePassword(password);
			kommissionierung.clickOnOK();  
		}
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Kommissionierung","Auswahl_Kommissionierung" ).contains("1")) {
			mainPage.clickMenueOption("Kommissionierung");
			kommissionierung.setLagerbereich();
		}
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Kommissionierung","Scanne_Komm_Gestell" ).contains("1")) {
			kommissionierung.setLeerenKommGestell();
		
		}
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Kommissionierung","Starte_Kommissionierung" ).contains("1")) {
			kommissionierung.clickOnOK();
			kommissionierung.picken();
		}
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Kommissionierung","Scanne_ET_Drucker" ).contains("1")) {
			kommissionierung.setDruckerID("1");
		}
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Kommissionierung","Starte_direkt_ Konsolidierung" ).contains("1")) {
		// Bei LITE nach dem Kommission den Supermarkt scannen
			//kommissionierung.scanneBeliebegeBoxNrvomGestell();
			kommissionierung.scanneSupermarkt();	
			kommissionierung.scanneBeliebegeBoxNrvomGestell();
			kon.konsolidiere();  
		}
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Kommissionierung","Abschluss" ).contains("1")) {
			kon.abschluss();
			}
	}
  
	
	@DataProvider
	public static Object[][] dataProviderAdd() {
		return new Object[][] { 
		{ "1", "1201", "1201" } 
		//, { "2", "1202", "1202" }, { "3", "1203", "1203" }, { "4", "1204", "1204" }, { "5", "1205", "1205" }, 
		//{ "6", "1206", "1206" }, { "7", "1207", "1207" }, { "8", "1208", "1208" }, { "9", "1209", "1209" }, { "10", "1210", "1210" }
		};}
	@DataProvider()
	public static Object[][] loginData() throws Throwable {
		Object[][] arrayObject = getExcelData(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","LoginData");
		return arrayObject;
	}

	@Test
	@Ignore
	public void testen() throws ConfigurationException {
		TestBase tBase = new TestBase();
		login.visit("http://172.31.0.42:62670/plbring/servlet/start?mde=001");
		System.out.println(tBase.readProperty("aktuelleKommZone"));
		System.out.println(dbCon.getPIDvomKommGestellVonDB("602700330", "BER"));

		//System.out.println(dbCon.getBereitsBelegtenTourenGestellVonDB("2078"));

		//dbCon.getBereitsBelegtenTourenGestellVonDB("2078");

	}

}

