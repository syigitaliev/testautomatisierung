package de.bring.pl_mobil;


import org.junit.Before;
import org.junit.Test;

import pageObjects.Konsolidierung;
import pageObjects.Login;
import pageObjects.MainPage;


public class TestKonsolidierung extends TestBase {
	private Login login;
	private MainPage mainPage; 
	//private Kommissionierung kommissionierung;
	private Konsolidierung konso; 
	
	@Before
	public void SetUp() throws Exception {  

		login = new Login(driver);
		mainPage = new MainPage(driver);
		//kommissionierung = new Kommissionierung(driver);
		konso = new Konsolidierung(driver);
	}
	@Test
	public void konsolidieren() throws Throwable {
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Konsolidierung","Standort" ).contains("BER")) {
		login.visit(readProperty("URL_BER")+"77");
		}
		
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Konsolidierung","Standort" ).contains("MUC")) {
			login.visit(readProperty("URL_MUC")+"30");
			}
		
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Konsolidierung","Einloggen" ).contains("1")) {
		login.typeUsername(readProperty("username"));
		login.typePassword(decrypt(readProperty("password")));
		login.clickLoginButton();
		}
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Konsolidierung","Auswahl_Kommissionierung" ).contains("1")) {
		
			
			mainPage.clickMenueOption("Konsolidierung");
		}
		
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Konsolidierung","Starte_Kommissionierung" ).contains("1")) {
		konso.konsolidiere();
		}
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Konsolidierung","Abschluss" ).contains("1")) {
		konso.abschluss();
		}
		
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Konsolidierung","Starte_Kommissionierung" ).contains("1")) {
		konso.konsolidiere();
		}
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Konsolidierung","Abschluss" ).contains("1")) {
		konso.abschluss();
		}
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Konsolidierung","Starte_Kommissionierung" ).contains("1")) {
		konso.konsolidiere();
		}
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Konsolidierung","Abschluss" ).contains("1")) {
		konso.abschluss();
		}
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Konsolidierung","Starte_Kommissionierung" ).contains("1")) {
		konso.konsolidiere();
			}
		if(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Konsolidierung","Abschluss" ).contains("1")) {
		konso.abschluss();
			}
}
	
	
}
