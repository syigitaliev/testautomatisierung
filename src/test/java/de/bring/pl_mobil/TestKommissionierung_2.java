package de.bring.pl_mobil;


import org.apache.commons.configuration.ConfigurationException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import com.tngtech.java.junit.dataprovider.*;
import pageObjects.Kommissionierung;
import pageObjects.Konsolidierung;
import pageObjects.Login;
import pageObjects.MainPage;

 @RunWith(DataProviderRunner.class)
public class TestKommissionierung_2 extends TestBase {
	private Login login; 
	private MainPage mainPage;
	
	private Kommissionierung kommissionierung;
	private ConnectToDB dbCon;
	private Konsolidierung kon;
	private static TestBase tBase;
	@Before
	public void SetUp() throws Exception {

		login = new Login(driver);
		mainPage = new MainPage(driver);
		kommissionierung = new Kommissionierung(driver);
		dbCon = new ConnectToDB();
		kon = new Konsolidierung(driver);
		tBase = new TestBase();
	}

	@Test
	@Ignore
    @UseDataProvider("dataProviderAdd")
	public void kommissioniere(String mde, String user, String password) throws Throwable {
		String tmp ="http://172.31.0.42:62670/plbring/servlet/start?mde="+mde;
		login.visit(tmp);
		login.typeUsername(user);
		login.typePassword(password);
		kommissionierung.clickOnOK();
		mainPage.clickMenueOption("Kommissionierung");
	//	kommissionierung.setKommZone();

		kommissionierung.setLeerenKommGestell();
		kommissionierung.clickOnOK();
		kommissionierung.picken();
		kommissionierung.setDruckerID("3");
		// Bei LITE nach dem Kommission den Supermarkt scannen
		kommissionierung.scanneSupermarkt();	
		kon.konsolidiere();  
		// /* kon.setLeereTourenGestellFachVonDB();
		// dbCon.getPIDvomKommGestellVonDB(kon.getKommGestellfachID()); */
	}
  
	@DataProvider
	public static Object[][] dataProviderAdd() {
		return new Object[][] { 
		{ "6", "1206", "1206" }, { "7", "1207", "1207" }, { "8", "1208", "1208" }, { "9", "1209", "1209" }, { "10", "1210", "1210" }};}
	
		/*ExcelDataConfig config = new ExcelDataConfig(System.getProperty("user.dir") + tBase.readProperty("excelPath"));
		int rows = config.getRowCount(0);
		Object[][] data = new Object[rows][2];

		for (int i = 0; i < rows; i++) {
			data[i][0] = config.getData(0, i, 0);
			data[i][0] = config.getData(0, i, 0);
		}
		return data; */
	

	/*@Ignore
	public Object[][] passData() {
		ExcelDataConfig config = new ExcelDataConfig(System.getProperty("user.dir") + readProperty("excelPath"));
		int rows = config.getRowCount(0);
		Object[][] data = new Object[rows][2];

		for (int i = 0; i < rows; i++) {
			data[i][0] = config.getData(0, i, 0);
			data[i][0] = config.getData(0, i, 0);
		}
		return data;
	}*/

	@Test
	@Ignore
	public void testen() throws ConfigurationException, Exception {
		TestBase tBase = new TestBase();
		String standort =readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Kommissionierung","Standort" );
		login.visit("http://172.31.0.42:62670/plbring/servlet/start?mde=001");
		System.out.println(tBase.readProperty("aktuelleKommZone"));
		System.out.println(dbCon.getPIDvomKommGestellVonDB("602700330", "BER"));

		System.out.println(dbCon.getBereitsBelegtenTourenGestellVonDB("2078", "11", "BER"));

		dbCon.getBereitsBelegtenTourenGestellVonDB("2078", "11", standort);

	}

}

