package de.bring.pl_mobil;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;

import pageObjects.Konsolidierung;
import pageObjects.Login;
import pageObjects.MainPage;
import pageObjects.Portionierung;

 @RunWith(DataProviderRunner.class)
 public class TestPortionierung extends TestBase {
	 	private Login login; 
	 	private MainPage mainPage;
	 	
	 	private Portionierung portionierung;
	 	private ConnectToDB dbCon;
	 	private Konsolidierung kon;
	 	private static TestBase tBase;
	 	@Before
	 	public void SetUp() throws Exception {

	 		login = new Login(driver);
	 		mainPage = new MainPage(driver);
	 		portionierung = new Portionierung(driver);
	 		dbCon = new ConnectToDB();
	 		kon = new Konsolidierung(driver);
	 		tBase = new TestBase();
	 	}

	 	@Test
		@Ignore 
	     @UseDataProvider("dataProviderAdd")
	 	public void kommissioniere(String mde, String user, String password) throws Throwable {
	 		String uRL ="http://172.31.0.42:62670/plbring/servlet/start?mde="+mde;
	 		String uRL_MUC = "http://172.31.0.58:62670/plbring/servlet/start?mde="+mde;
	 		login.visit(uRL);
	 		login.typeUsername(user);
	 		login.typePassword(password);
	 		login.clickLoginButton();
	 		mainPage.clickMenueOption("Portionierung");
	 		

	 		

	 	}
	   
	 	@DataProvider
	 	public static Object[][] dataProviderAdd() {
	 		return new Object[][] { 
	 		{ "800", "1401", "1401"} 
	 		//, { "2", "1202", "1202" }, { "3", "1203", "1203" }, { "4", "1204", "1204" }, { "5", "1205", "1205" }, 
	 		//{ "6", "1206", "1206" }, { "7", "1207", "1207" }, { "8", "1208", "1208" }, { "9", "1209", "1209" }, { "10", "1210", "1210" }
	 		};}

}
