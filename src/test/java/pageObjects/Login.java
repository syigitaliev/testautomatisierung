package pageObjects;




import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import de.bring.pl_mobil.Base;



public class Login extends Base{

	@FindBy(how = How.ID, using = "eingabe1")
	private WebElement userNameLocator;
//	private By userNameLocator = By.id("eingabe1");
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"header\"]/table/tbody/tr[1]/td[3]")
	private WebElement mde_ID_Locator;
	
	@FindBy(how=How.ID, using ="eingabe2")
	private WebElement passwordLocator;
//	private By passwordLocator = By.id("eingabe2");
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"steuerung\"]/table/tbody/tr/td[3]/input")
	private WebElement loginBottonLocator;
	
/*  @FindBy(how = How.CSS, using = ".ftaste_vor")
	private WebElement loginBottonLocator;
*/
	//private By loginBottonLocator = By.xpath("//input[@type='submit']");
//	private By loginBottonLocator = By.cssSelector(".ftaste_vor");

	
	@FindBy(how = How.CSS, using = "#fkt_1")
	private WebElement loginBottonWaageLocator;
	
	@FindBy(how =How.CSS, using = ".ftaste_zurueck")
	private WebElement logoutButtonLocator;

	//private By logoutButtonLocator = By.cssSelector(".ftaste_zurueck");
	
	public Login(WebDriver driver){
		super(driver);
		
	}
	
	public void typeUsername(String  txtUserName){
		type(userNameLocator, 4, txtUserName);
		
	}
	
	public void typePassword(String  txtPassword){
		type(passwordLocator, 4, txtPassword);
	}
	public void clickLoginButton(){
		//getMDE_Art(mde_ID_Locator, 3);
	//	if(!istWaage()) {
			click(loginBottonLocator, 3);
		
	}
	public void clickLogoutButton(){
        click(logoutButtonLocator, 3);

}
	

}

