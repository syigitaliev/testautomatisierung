package pageObjects;



import java.util.Random;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import de.bring.pl_mobil.Base;
import de.bring.pl_mobil.ConnectToDB;


public class Konsolidierung extends Base {
	String kommGestellfachID;
	WebDriver driver;
	private String aktuellerKommGestell;
	private String tourBezeichnung;
	String standort;
//	private TestBase tBase;
	//private Kommissionierung kom;
	ConnectToDB conToDB;
	String tourenGestellFachID;
	String gestellfach;
	String tourenGestell;
	String tourenGestellFach;
	String aktuelleTourenGestell;
	// List <String> gestellListe;
	String aktuellePID = null;
	boolean istTGVorhanden = false;

/*	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[7]/td[2]")
	private WebElement tourenGestellLocator;*/

	@FindBy(how = How.XPATH, using = "//*[@id='liste']/table/tbody/tr[1]/td/table/tbody/tr/td[1]")
	private WebElement kommGestellLocatorvom7304;

	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[3]/td[2]")
	private WebElement kommGestellLocatorvom7310;

	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[3]/td[2]")
	private WebElement kommGestellLocatorvom7305;
	
	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[1]/td[2]")  
	private WebElement tourBezeichungLocator7305;

	@FindBy(how = How.XPATH, using = "//*[@id='liste']/table/tbody/tr[1]/td/table/tbody/tr/td[2]")
	private WebElement gestellfachLocator;

	@FindBy(how = How.XPATH, using = "//td[@class='wertgross']")
	private WebElement kommSpurLocator;
	
	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[2]/td[2]")
	private WebElement wsSpurLocator;

	@FindBy(how = How.XPATH, using = "//input[@type='submit']")
	private WebElement buttonOKLokator;
	
	@FindBy(how = How.XPATH, using = "//*[@class='ftaste_zurueck']")
	private WebElement buttonZurückLokator;
	
	@FindBy(how = How.XPATH, using = "//input[@type='submit' and @class='ftaste_vor']")
	private WebElement buttonJaSubmitLokator;
	
	@FindBy(how = How.XPATH, using = "//input[@type='button' and @class='ftaste_vor']")
	private WebElement buttonJaButtonLokator;
	
	@FindBy(how = How.ID, using = "eingabe1")
	private WebElement inputTextLocator;

	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table[1]/tbody/tr[3]/td[2]")
	private WebElement belegteKommGestellLocator;

	@FindBy(how = How.XPATH, using = "//*[@id='header']/table/tbody/tr[1]/td[1]/img")
	private WebElement proLogistikLogo;
	
	@FindBy(how = How.XPATH, using = "//*[@id='eingabe']/table/tbody/tr[2]/td")
	private WebElement abschlussFrageLocator;
	
	@FindBy(how = How.XPATH, using = "//*[@id='eingabe']/table[1]/tbody/tr/td[1]")
	private WebElement zielGestellOderPaletteLocator;
	
	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[7]/td[2]")
	private WebElement gestellVorschlagLocator;
	
	@FindBy(how = How.XPATH, using = "//*[text()='Fach:']/following::td[1]")
	private WebElement gestellVorschlagTGFachLocator;
	
	@FindBy(how = How.XPATH, using = "//*[@id='steuerung']/table/tbody/tr/td[2]/input")
	private WebElement weiterButtonLocator;
	
	@FindBy(how =How.CSS, using = ".ftaste_zurueck")
	private WebElement logoutButtonLocator;
	
	@FindBy(how = How.XPATH, using = "//td[@class='hinweis']")
	private WebElement hinweisPIDLocator;
	
	@FindBy(how = How.XPATH, using = "//td[(text()= 'Zielfach: ')]/following::td[1]")
	private WebElement zielFachPufferLokator;
	
	@FindBy(how = How.XPATH, using = "//td[@class='hinweis']")
	private WebElement hinweisLocator;
	
	public static void main(String[] args) {
		
	
	}
	
	
	// Konstruktor
	public Konsolidierung(WebDriver driver) throws Exception {
		super(driver);
		// kom = new Kommissionierung(driver);
		conToDB = new ConnectToDB();
		 standort =readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx","Kommissionierung","Standort" );
	//	tBase = new TestBase();
		// gestellListe = new ArrayList<String>();
	}

	// Methoden zur Ermittelung von KOM-Gestellfach_IDs neben PIDs. Anhand der
	// ermittelten Fach-ID wird in anderer Methode PIDs abgefragt.
	public  String getKommGestellfachID() {
		waitForElementToBeVisible(proLogistikLogo, 10);
		if (checkMaskeID(7304) || checkMaskeID(4264) ) {
			String str = gestellfachLocator.getText();
			System.out.println("Das ist getText Gestellfachnummer: " + str);
			str = str.replace("-", "");
			String kommGestell = getKommGestell();
	// TODO
			if(kommGestell.substring(0,1).equals("2")) {
				kommGestell.substring(0,1);
			kommGestell = kommGestell.replaceFirst("2", "6");
			}
			if(kommGestell.substring(0,1).equals("3")) {
				kommGestell.substring(0,1);
			kommGestell = kommGestell.replaceFirst("3", "7");
			}
			kommGestellfachID = kommGestell + "0" + str;
			System.out.println("Das ist aktuelle Gestellfachnummer: " + kommGestellfachID);
			return kommGestellfachID;
		} else {
			System.out.println("Kein Fach");
			return null;
		}

	}

	public String getKommGestell() {
		if (checkMaskeID(7304)) {
			String str = kommGestellLocatorvom7304.getText();
			aktuellerKommGestell = str.substring(0, 4);
			return aktuellerKommGestell;

		} else if (checkMaskeID(7310)) {
			aktuellerKommGestell = kommGestellLocatorvom7310.getText();
			return aktuellerKommGestell;
		}

		else if (checkMaskeID(7305)) {
			aktuellerKommGestell = kommGestellLocatorvom7305.getText();
			return aktuellerKommGestell;
		} 
		else if (checkMaskeID(4264)) {
			String str = kommGestellLocatorvom7304.getText();
			aktuellerKommGestell = str.substring(0, 4);
			return aktuellerKommGestell;
		}
		else
			
			return "Kein Komm-Gestell";
		
	}

	public String getAktuellerKommGestell() {
		return aktuellerKommGestell;
	}
	
 //Custing TourenGestellfach zu einer TourenGestellfachID
	/*
	 * public String getTourenGestellfachID(String tourenGestell) {
	 * 
	 * tourenGestell=tourenGestell.replace("-", ""); if(standort.contains("BER")) {
	 * tourenGestell = tourenGestell.replaceFirst("4", "4");
	 * 
	 * tourenGestell = tourenGestell + "00111";
	 * 
	 * } else { tourenGestell = tourenGestell + "00110"; }
	 * System.out.println("Das ist aktuelle TG-fachnummer: " + tourenGestell);
	 * 
	 * return tourenGestell;
	 * 
	 * }
	 */
	
	// Overriding getTourenGestellfachID(String tourenGestell)
	public String getTourenGestellfachID(String tourenGestell, String tourenGestellfach) {
		tourenGestell = tourenGestell + "0" + tourenGestellfach;
		tourenGestell = tourenGestell.replace("-", "");
		return tourenGestell;
	}

	// Gibt Kommspur zurück
	public String getKommSpur() {

		String tmp = kommSpurLocator.getText();
		tmp = tmp.replace("-", "") + "00000";
		return tmp;

	}
	// check Texthinweis
	public boolean checkTexthinweis(String text) {

	if (text.equals(hinweisLocator.getText())) {  return true;}
	else return false;

	}
	
	
	// Gibt WSspur zurück
	public String getWSSpur() {
		String tmp = wsSpurLocator.getText();
		tmp = tmp.replace("-", "") + "00000";
		return tmp;

	}

	// Konsolidierung - Ende?
	public void endeKonsolBestaetigen() {
	try {
		if (checkMaskeID(7307)) {
			clickOnJa();
		}
	} catch (NoSuchElementException e) {
		clickOnOK();
	}
	}

	// Aus vierstelligen TG-Nummer wird ein TG-fachnummer gemacht und
	// zurückgegeben
	/*
	 * public String getTGPlatz(String str) { if (standort.contains("BER")) { str =
	 * str + "00110"; } else { str = str.replaceFirst("4", "4"); str = str +
	 * "00111"; } return str; }
	 */
// diese Methode soll mit der DB-Query erweitert werden. 
	public String getBelegtenKommGestell() {
		/*
		 * String str = belegteKommGestellLocator.getText(); if (str != null &&
		 * !str.isEmpty()) { aktuellerKommGestell = str.substring(0, 4); return
		 * aktuellerKommGestell;
		 * 
		 * } else if( str.isEmpty()) {return
		 * conToDB.getBelegteKommGestellVonDB(standort);} else {
		 * 
		 * return "Keine belegte KG"; }
		 */
	
		return conToDB.getBelegteKommGestellVonDB(standort);
	
	}
	// Diese Methode fragt zunächst die DB nach leeren Tourengestell und setzt
	// den ins Input-Feld
	/*
	 * public void setLeereTourenGestellFachVonDB() { if ( checkMaskeID(7310)
	 * ||checkMaskeID(7305)) { // // Hole von der DB einen leeren TG und wandele ihn
	 * über die Methode // getTGPlatz zu Gestellfach_ID tourenGestellFachID =
	 * getTGPlatz(conToDB.getLeerTourenGestellVonDB(standort));
	 * System.out.println("setLeereTourenGestellFachVonDB() = "+tourenGestellFachID)
	 * ; // Setze die Gestellfach_ID in den Input-Box und bestätige mit OK
	 * inputText(tourenGestellFachID); clickOnOK(); }
	 * 
	 * }
	 */

	public boolean nextKommGestell() {
		waitForElementToBeVisible(proLogistikLogo, 10);
		if (checkMaskeID(7304) || checkMaskeID(4264)) {
			String str = getKommGestell();
			if (str.contentEquals("Kein Komm-Gestell")) {
				return false;
			} else {
			System.out.println("Nexte Gestell: " + str);
			str = str.substring(0, 1);
			if (str.equals("2") || str.equals("6") ) {
				return true;
			} else
				return false;
			}
		} else
			return false;

	}

	public void konsolidiere() {
 
		
		
		System.out.println("Konsolidierung wurde aufgerufen");
		waitForElementToBeVisible(inputTextLocator, 10);
		
		//scanneBeliebegeBoxNrvomGestell();
		
		kommSpurScannen();
		if (checkMaskeID(7302) && getBelegtenKommGestell()!=null) {
			inputText(getBelegtenKommGestell());
			clickOnOK();
		}
		
		
// TODO  hier geht es weiter		
		boxZuTourengestell();
		
		/*
		 * // bei Liefery PID scannen if (checkMaskeID(7305) || checkMaskeID(7310)) { //
		 * Maske 7310 - über Konsolidierung und Maske 7305 - Direktkonsolidierung
		 * 
		 * String str = zielGestellOderPaletteLocator.getText(); // istTGVorhanden =
		 * gestellVorschlagLocator.isEnabled(); if(str.equals("Zielfach")){
		 * setLeereTourenGestellFachVonDB(); } else if(str.equals("Palette")){ // Die
		 * letzten 4 Zahlen werden per Random erzeugt und als PID vergeben.
		 * inputText("777777"+getRandom(1001, 9999)); clickOnOK(); }
		 * 
		 * try { if (checkMaskeID(7310) && !gestellVorschlagLocator.getText().isEmpty())
		 * { inputText(getTGPlatz(gestellVorschlagLocator.getText())); clickOnOK(); } }
		 * catch (NoSuchElementException e) { setLeereTourenGestellFachVonDB(); }
		 * 
		 * 
		 * }
		 */
		// Bei LITE Etiketten drucken
		if (checkMaskeID(7311)) {

			inputText("1");
			clickOnOK();
			// setLeereTourenGestellFachVonDB();

		}

		/*
		 * if (checkMaskeID(7304)) { clickOnZurück(); }
		 */
		
		endeKonsolBestaetigen();
		
		// Auswertung Pickleistung
		clickAufWeiter();
		
		//Ausloggen
		logout();
	}
	
	
	public void boxZuTourengestell()  {
	
	while (nextKommGestell() || checkMaskeID(7305)  || checkMaskeID(7310) || checkMaskeID(7304) ||checkMaskeID(7303)) {   
	
		kommSpurScannen();
		
			if (checkMaskeID(7304) || checkMaskeID(4264)) {
				System.out.println("Hier ist die ID vom Gestellfach: "+getKommGestellfachID());
				aktuellePID = conToDB.getPIDvomKommGestellVonDB(getKommGestellfachID(), standort);
				System.out.println("Hier ist die PID vom Gestell: "+aktuellePID);
				inputText(aktuellePID);
				clickOnOK();
			}
			if(checkMaskeID(4265)) {
				String tmp=zielFachPufferLokator.getText();
				tmp=tmp.replace("-", "");
				inputText(tmp);
				clickOnOK();
			}
			
// TODO  Hier muss noch Konsilidierung für die LVS-681 angepasst werden 			
			if (checkMaskeID(7305) || checkMaskeID(7310)) {
				tourBezeichnung=tourBezeichungLocator7305.getText();	
				getTGZielfach();
				
				/*
				 * aktuelleTourenGestell =
				 * conToDB.getBereitsBelegtenTourenGestellVonDB(getKommGestell(),
				 * tourBezeichnung, standort); if( aktuelleTourenGestell!=null){
				 * System.out.println("getBereitsBelegtenTourenGestellVonDB()= " +
				 * aktuelleTourenGestell);
				 * inputText(getTourenGestellfachID(aktuelleTourenGestell)); clickOnOK();
				 * 
				 * 
				 * }else { setLeereTourenGestellFachVonDB(); }
				 */
				
				
			}
		
			
			/*
			 * if (checkMaskeID(7310)) { // TODO tourBezeichungLocator7305 muss eventuell
			 * für die Maske 7310 angepasst werden
			 * tourBezeichnung=tourBezeichungLocator7305.getText(); aktuelleTourenGestell =
			 * conToDB.getBereitsBelegtenTourenGestellVonDB(getKommGestell(),
			 * tourBezeichnung, standort);
			 * inputText(getTourenGestellfachID(aktuelleTourenGestell)); clickOnOK(); }
			 */
			
		}
	
	}
	
	public void logout() {
		if (checkMaskeID(4200)) {
			logoutButtonLocator.click();
		}
		if (checkMaskeID(200)) {
			logoutButtonLocator.click();
		}
		
	}
	
	
	public void abschluss() throws Throwable{
		System.out.println("Abschluss wurde gestartet");
	
		clickAufJabeiMehrTG();
		
		endeKonsolBestaetigen();
		
		if (checkMaskeID(7308)) {
			inputText(getWSSpur());
			System.out.println("WS-Spur wurde eingegeben ");
			clickOnOK();
		}
		if (checkMaskeID(7303)) {
			
			konsolidiere();
		
		}
		if (checkMaskeID(7302)) {
			
			System.out.println("Konsolidierung abgeschlossen!");
		
		}
		// Auswertung Pickleistung
		clickAufWeiter();
	}

	public void inputText(String text) {

		// System.out.println(kommZone);
		type(inputTextLocator, 3, text);
	}

	public void clickOnOK() {
		
		click(buttonOKLokator, 5);

	}
	
	public void clickOnJa() {
	
		if(checkMaskeID(7307)) {
			click(buttonJaButtonLokator, 5);
			}
		
		click(buttonJaSubmitLokator, 5);
		 
	}
	
	public void clickOnZurück() {
		click(buttonZurückLokator, 5);

	}
	public void kommSpurScannen(){
	if (checkMaskeID(7303)) {
		inputText(getKommSpur());
		System.out.println("Kommspur wurde eingegeben ");
		clickOnOK();
		
	}
		
		  if (checkMaskeID(7303)) { String
		  wsSpur=conToDB.getWS_SpurVonDB(getKommSpur(), standort);
		  inputText(wsSpur.substring(3,12)); clickOnOK();
		  
		  }
		 

	}
	
	public void clickAufWeiter() {
		if (checkMaskeID(9031)) {
			
		}
		if (checkMaskeID(4109)) {
			waitForElementToBeVisible(weiterButtonLocator, 6);
			weiterButtonLocator.click();
		}
		
	}
	
	public static int getRandom(int from, int to) {
	    if (from < to)
	        return from + new Random().nextInt(Math.abs(to - from));
	    return from - new Random().nextInt(Math.abs(to - from));
	}
	
	public void clickAufJabeiMehrTG(){
		if (checkMaskeID(7333)) {
			clickOnJa();
			konsolidiere();
		}
	}
	
	
	
	
	
	
	
	
	// TODO Konsolidierung
	
	public void getTGZielfach() {
		try {
			
			
			if (checkMaskeID(7305) || checkMaskeID(7310) ) /* && gestellVorschlagLocator.getText().isEmpty() */ {
				tourenGestellFach = gestellVorschlagTGFachLocator.getText();
				if(tourenGestellFach.contains("00-")) {
					String tmp[]=tourenGestellFach.split("00-");
					tmp[1]=tmp[1].replace(" ", "");
					tmp[1]=tmp[1].replace("&", "");
					 tourenGestell =conToDB.getLeerTourenGestellVonDB(standort)+"001"+tmp[1];
				//	if(tourenGestell.substring(0,1).equals("4") && standort.equals("BER")) {
					//	tourenGestellFach = tourenGestell.replaceFirst("4", "4");}
					inputText(tourenGestell);
					
					clickOnOK();
								
				}
			}
			if ((checkMaskeID(7305) || checkMaskeID(7310))&& !gestellVorschlagLocator.getText().isEmpty()) {
				 tourenGestell = gestellVorschlagLocator.getText();
			//	if(tourenGestell.substring(0,1).equals("4") && standort.equals("BER")) {
			//		tourenGestell = tourenGestell.replaceFirst("4", "4");	}
			
		
				try {
					if ((checkMaskeID(7305) || checkMaskeID(7310)) && !hinweisPIDLocator.getText().isEmpty()) {
					 tourenGestellFach = gestellVorschlagTGFachLocator.getText();
					
					 tourenGestellFach=getTourenGestellfachID(tourenGestell, tourenGestellFach);
					 String pid =hinweisPIDLocator.getText();
					 pid=pid.replace("Bitte PID ...", "");
					 pid=pid.replace(" scannen", "");
					 
					 
					 pid =conToDB.getPIDvomTourenGestellVonDB(tourenGestellFach, pid, standort);
					 
					 inputText(pid);
					 
					
					}
						clickOnOK();
					
				} catch (NoSuchElementException e) {
			// hier ist Platzhalter
					if (checkMaskeID(7305) || checkMaskeID(7310) ) {
						 tourenGestellFach = gestellVorschlagTGFachLocator.getText();
						
						 tourenGestellFach=getTourenGestellfachID(tourenGestell, tourenGestellFach);
						 inputText(tourenGestellFach);
						 
						}
							clickOnOK();	
				}
					
			}
			
			} catch (NoSuchElementException e) {
				// setLeereTourenGestellFachVonDB();
				System.out.println("Bitte entkommentieren:  setLeereTourenGestellFachVonDB();");
		        }

}
	/*
	 * public void scanneBeliebegeBoxNrvomGestell() { if(checkMaskeID(4080) &&
	 * (checkTexthinweis("Beliebiges Etikett scannen") ||
	 * checkTexthinweis("Alle Etiketten sind jetzt aufgeklebt."))) {
	 * inputText(conToDB.getBeliebegeBoxNrvomGestellFachVonDB(getKommGestell(),
	 * "BER")); clickOnOK(); } }
	 */
	
	
	
}