package pageObjects;


import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import de.bring.pl_mobil.Base;


public class MainPage extends Base{

	// Initialisierung von Locators 
	@FindBy(how= How.CLASS_NAME, using = "paramwinzig")
	private WebElement maske_ID_Locator;
	
	@FindBy(how=How.XPATH, using ="//div[@id='uebersicht']/table")
	private WebElement menueTable;
		
	@FindBy(how=How.XPATH, using ="//input[@type='button' and @class='ftaste_vor' and @value='Nächste']")
	private WebElement vorButtonLocator;
	
	@FindBy(how=How.XPATH, using ="//input[@type='button' and @class='ftaste_vor' and @value='vor']")
	private WebElement nextButtonLocator;
//____________________________________________________________________________________________
	// Definition der Konstruktors  
	public MainPage(WebDriver driver) {
		super(driver);
	}
//____________________________________________________________________________________________
	// Definition von  Methoden
	@Override
	public boolean checkMaskeID(int  ID){
		return checkMaske(maske_ID_Locator, ID, 4);
	}
	
	public void clickMenueOption( String text){
		try {
		if(checkMaskeID(200)){
		String tmp ="//input[@class='menuebutton' and @value ='"+text+"']";
		WebElement menueOptions= menueTable.findElement(By.xpath(tmp));
		click(menueOptions, 4);
		}
	
	} catch (NoSuchElementException e) {
		try {
		click(vorButtonLocator, 4);
		if(checkMaskeID(200)){
			String tmp ="//input[@class='menuebutton' and @value ='"+text+"']";
			WebElement menueOptions= menueTable.findElement(By.xpath(tmp));
			click(menueOptions, 4);
		}	
		} catch (NoSuchElementException e2) {
			click(vorButtonLocator, 4);
			if(checkMaskeID(200)){
				String tmp ="//input[@class='menuebutton' and @value ='"+text+"']";
				WebElement menueOptions= menueTable.findElement(By.xpath(tmp));
				click(menueOptions, 4);
			
		}
		
	}
		
		
	}
		
	}

}

