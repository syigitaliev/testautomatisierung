package pageObjects;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.configuration.ConfigurationException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import de.bring.pl_mobil.Base;
import de.bring.pl_mobil.ConnectToDB;
import de.bring.pl_mobil.TestBase;
import de.bring.pl_mobil.TestKommissionierung;

public class Kommissionierung extends Base {
	String kommZone;
	String[] gestellArtikelMengePlatz = new String[10];
	String gestell;
	String artikel;
	String menge;
	String lagerplatz;
	TestBase tBase;
	Login l;
	ConnectToDB conToDB;
	String standort;
	String maske = "0";
	String nextMaske = "";
	long index = 10;
	String versandart;	
	
	String gestellfach = "";
	List<String> list; // = Arrays.asList("B", "A", "A", "C", "B", "A");
	// Initialisierung von Locators

	@FindBy(how = How.CLASS_NAME, using = "paramwinzig")
	private WebElement restMengen_Locator;

	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[4]/td")
	private WebElement KommZoneLocator;

	@FindBy(how = How.XPATH, using = "//*[@id=\'uebersicht\']/table/tbody/tr[1]/td")
	private WebElement KommZoneGETLocator;

	@FindBy(how = How.ID, using = "eingabe1")
	private WebElement inputTextLocator;

	@FindBy(how = How.XPATH, using = "//*[@class='ftaste_vor' and @value='Ok']")
	private WebElement buttonOKLokator;

	@FindBy(how = How.XPATH, using = "//*[@class='ftaste_vor' and @value='Fertig']")
	private WebElement buttonFerigLokator;

	@FindBy(how = How.XPATH, using = "//*[@class='ftaste_zurueck' and @value='Neue Box']")
	private WebElement buttonNeuerBoxLokator;

	@FindBy(how = How.XPATH, using = "//input[@type='submit' and @class='ftaste_vor']")
	private WebElement buttonJaSubmitLokator;
	
	@FindBy(how = How.XPATH, using = "//input[@type='button' and @class='ftaste_vor']")
	private WebElement buttonJaButtonLokator;

	// @FindBy(how = How.XPATH, using =
	// "//*[@id='uebersicht']/table/tbody/tr[1]/td[2]")
//	private WebElement gestellfachLocator;

	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[4]/td[2]")
	private WebElement gestellfachFuerTueteLocator;

	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[2]/td[2]")
	private WebElement artikelNrLocator;

	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[5]/td[2]")
	private WebElement lagerplatzLocator;

	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[4]/td[2]")
	private WebElement mengenLocator;

	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[3]/td[2]")
	private WebElement supermarktLocator;

	@FindBy(how = How.XPATH, using = "//input[@type='submit' and @class='ftaste_vor']")
	private WebElement weiterButtonLocator;

	@FindBy(how = How.XPATH, using = "//*[@class='wert']")
	private WebElement tuetenPIDOrGestellLocator;

	@FindBy(how = How.XPATH, using = "//*[@class = 'wertgross' and @colspan='3']")
	private WebElement bedienthekeQuellPlatzLokator;

	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[5]/td[2]/span")
	private WebElement zielGestellfachLokator;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"uebersicht\"]/table/tbody/tr[4]/td[2]/span")
	private WebElement zielGestellfach_4268Lokator;
	
	
	
	// Backup "//td[(text()= 'Zielfach: ')]/following-sibling::td"
	@FindBy(how = How.XPATH, using = "//*[@id='uebersicht']/table/tbody/tr[5]/td[2]/span")
	private WebElement zielGestellfach2Lokator;

	@FindBy(how = How.XPATH, using = "//td[(text()= 'Gewicht:')]/preceding::td[1]")
	private WebElement gewichtLokator;

	@FindBy(how = How.XPATH, using = "//*[@id=\"uebersicht\"]/table/tbody/tr[1]/td[2]/span")
	private WebElement zielGestellLokator;

	@FindBy(how = How.XPATH, using = "//*[@id='header']/table/tbody/tr[1]/td[3]")
	private WebElement mdeNRlLokator;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"uebersicht\"]/table/tbody/tr[2]/td[2]")
	private WebElement PIDLokator;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"uebersicht\"]/table/tbody/tr[3]/td[2]")
	private WebElement tourBezLokator;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"uebersicht\"]/table/tbody/tr[3]/td[4]")
	private WebElement ladefolgeLokator;
	// Backup
	String aktuellerZ_Platz =null;
	
	@FindBy(how = How.XPATH, using = "//td[@class='hinweis']")
	private WebElement hinweisLocator;

	// ____________________________________________________________________________________________
	// Definition der Konstruktors
	public Kommissionierung(WebDriver driver) throws Exception {
		super(driver);
		tBase = new TestBase();
		conToDB = new ConnectToDB();
		standort = readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx",
				"Kommissionierung", "Standort");
		index = Long.valueOf(readDataFromExcel(System.getProperty("user.dir") + "\\src\\main\\resources\\testdata.xlsx",
				"Kommissionierung", "BoxVoll bei X (X von 1 bis 100)"));
		l = new Login(driver);

	}

	// ____________________________________________________________________________________________
	// Definition von Methoden

	public void kommPlatzBesetzt() {
		// MaskenID Korrigieren
		if (checkMaskeID(4220)) {

			clickOnOK();

		}

	}

	public void setLagerbereich() {
	
		// TODO der folgende IF Block sollte erneuet geprüft werden:
		if (!checkMaskeID(4264) && !checkMaskeID(7303)) {
		String aktuellerQ_Platz = conToDB.getAktuellerQ_PlatzVonDB(getMDENR(), standort);
		// Erneuter Check der Zone, falls hier null zurückkommt
		for(int i = 0; aktuellerQ_Platz==null && i<5; i++ ) { 
			aktuellerQ_Platz = conToDB.getAktuellerQ_PlatzVonDB(getMDENR(), standort); }
		System.out.println("Aktueller Q_Platz ist " + aktuellerQ_Platz);
		if (aktuellerQ_Platz != null)
			kommZone = aktuellerQ_Platz.substring(3, 5);
		}
	}

	/*
	 * public void setZielplatzAmKG() {
	 * 
	 * if (checkMaskeID(4215)) { String aktuellerZ_Platz =
	 * conToDB.getAktuellerZ_PlatzVonDB(getMDENR(), standort); // Erneuter Check der
	 * Zone, falls hier null zurückkommt for(int i = 0; aktuellerZ_Platz==null &&
	 * i<10; i++ ) { aktuellerZ_Platz = conToDB.getAktuellerZ_PlatzVonDB(getMDENR(),
	 * standort); } System.out.println("Aktueller Z_Platz ist " + aktuellerZ_Platz);
	 * if (aktuellerZ_Platz != null) gestell = aktuellerZ_Platz.substring(3, 7); } }
	 */
	

	
	public String getKommZone() {
		return kommZone;
	}

	public void inputText(String text) {

		// System.out.println(kommZone);
		type(inputTextLocator, 5, text);
	}

	
	  public void clickOnOK() {
	  
	  click(buttonOKLokator, 5);
	  
	  }
	 

	// Fertig-Button klicken
	public void clickFertig() {
		click(buttonFerigLokator, 5);
	}

	// Rückgabe Gestellfach
	public String getKommGestellfach() {

		String tmp = "";
		if (checkMaskeID(4262)) {

			tmp = tuetenPIDOrGestellLocator.getText() + gestellfachFuerTueteLocator.getText();

		}

		if (checkMaskeID(4221) ) {
			
			 String [] str =zielGestellfachLokator.getText().split(" ");
			tmp = getKommGestell() + str[0];
			tmp = custingGestellfach(tmp);

		}
		else if ( checkMaskeID(4268)) {

			tmp = conToDB.getGestellNrVonDB(getMDENR(), "BER") + zielGestellfach_4268Lokator.getText();
			tmp = custingGestellfach(tmp);

		}
		if (checkMaskeID(4203)) {
			String test = zielGestellfach2Lokator.getText();
			System.out.println(test);
			if (!zielGestellfach2Lokator.getText().equals("")) {
				String [] str =zielGestellfach2Lokator.getText().split(" ");
				tmp = getKommGestell() +str[0];
			} else {
                String [] str =zielGestellfachLokator.getText().split(" ");
				tmp = getKommGestell() +str[0] ;
			}

			// stmp=custingGestellfach(tmp);

			if (tmp.equals("-")) {
				System.out.println("Das ist wahrscheinlisch Wertebereich");
				return tmp;
			} else {
				tmp = custingGestellfach(tmp);
			}
		}
		return tmp;
	}

	// Rückgabe Artikelnummer
	public String saveArtikelNr() {
		// System.out.println(artikelNrLocator.getText());
		return artikelNrLocator.getText();
	}

	// Custing Komm-Gestellfächer
	public String custingGestellfach(String tmp) {

		if (tmp.contains("-")) {
			tmp = tmp.replace("-", "");
		}
		if (standort.contains("BER")) {
			if (tmp.substring(0, 1).equals("2")) {

				tmp = tmp.replaceFirst("2", "6");
			}
		}
		tmp = tmp.substring(0, 4) + "0" + tmp.substring(4, tmp.length());
		System.out.println("Das ist aktuelle Gestellfachnummer: " + tmp);
		return tmp;
	}

	// Custing Touren-Gestellfächer
	public String custingTourenGestellfach(String tmp) {

		if (tmp.contains("-")) {
			tmp = tmp.replace("-", "");
		}
		if (standort.contains("BER")) {
			if (tmp.substring(0, 1).equals("2")) {

				tmp = tmp.replaceFirst("4", "4");
			}
		}
		tmp = tmp.substring(0, 4) + "0" + tmp.substring(4, tmp.length());
		System.out.println("Die Touren-Gestellfachnummer nach dem Custing: " + tmp);
		return tmp;
	}

	// Custing Touren-Gestellfächer
	public String getMDENR() {
		String tmp = mdeNRlLokator.getText();
		tmp = tmp.substring(0, 3);
		if (tmp.contains("/")) {
			tmp = tmp.replace("/", "");
		}

		System.out.println("Die MDE-Nr: " + tmp);
		return tmp;
	}

	// Rückgabe Lagerplatz
	public String saveLagerplatzNr() {
		String LagerplatzID = null;
		String lagerplatz = lagerplatzLocator.getText();

		LagerplatzID = getKommZone() + lagerplatz.replace("-", "");
		System.out.println("LagerplatzID ist " + LagerplatzID);

		return LagerplatzID;
	}

	// Rückgabe der zu pickender Menge
	public String saveMenge() {
		String tmp = mengenLocator.getText();
		String str[];
		str = tmp.split(" ");
		menge = str[0].toString();
		return menge;
	}

	public void setInfoGFachArtikelMengePlatz() {
		if (checkMaskeID(4201)) {
			// this.gestell = getKommGestellfach();
			this.artikel = saveArtikelNr();
			this.menge = saveMenge();
			this.lagerplatz = saveLagerplatzNr();

		} else {
			System.out.println("Falsche Maske");
		}
	}

	public String getKommGestell() {
		if (checkMaskeID(4203)) {

			gestell = zielGestellLokator.getText();
			
		}
		else if ( checkMaskeID(4268)) {

			gestell = zielGestellfach_4268Lokator.getText();
			
		}
		
		
		if (checkMaskeID(4221) && aktuellerZ_Platz==null ) {
			aktuellerZ_Platz = conToDB.getAktuellerZ_PlatzVonDB(getMDENR(), standort);
			// Erneuter Check der Zone, falls hier null zurückkommt
			if(aktuellerZ_Platz==null) { 
				aktuellerZ_Platz = conToDB.getAktuellerZ_PlatzVonDB(getMDENR(), standort); }
			System.out.println("Aktueller Z_Platz ist " + aktuellerZ_Platz);
			if (aktuellerZ_Platz != null)
				gestell = aktuellerZ_Platz.substring(3, 7);
			}

		if(checkMaskeID(4080)) {
			gestell=conToDB.getGestellNrVonDB(getMDENR(), "BER");
			System.out.println("Gestelle von der DB: " +gestell);
		}
		
		
		return gestell;
	}

	public String getArtikel() {
		return artikel;
	}

	public String getMenge() {
		return menge;
	}

	public String getLagerplatz() {
		return lagerplatz;
	}

	public void picken() throws Throwable {

		setDruckerID("1");
		// eventuell Supermarkt scannen
		scanneSupermarkt();

		// So lange picken, bis die Maske Drucker- oder Supermarkt-Scannen erreicht
		// wurde
		while (!checkMaskeID(4015) && !checkMaskeID(4204) && !checkMaskeID(4205) && !checkMaskeID(4200) && !checkMaskeID(4081)  && !checkMaskeID(7303) && !checkMaskeID(4264) && !checkMaskeID(4080)) {

			maske = "start";

			// Nulldurchgang und Eingabe einen Restmenge von 55 STK
			if (checkMaskeID(4010)) {
				inputText("55");
				//maske = "4010";
				clickOnOK();
			}

			//
			gewichtEingeben();

			// Wenn der Komm_Platz besetzt ist, dann wird 5 Sec. gewartet und dann auf OK
			// gekickt.
			kommPlatzBesetzt();

			// Implementierung OUG-Wiege im OST Kommissionierung

			if (checkMaskeID(4261)) {

				inputText(tuetenPIDOrGestellLocator.getText());

				clickOnOK();

				inputText(custingGestellfach(getKommGestellfach()));
				maske = "4261";
				clickOnOK();

			}

			if (checkMaskeID(4201)) {
				setInfoGFachArtikelMengePlatz();

				inputText(getLagerplatz());
				maske = "4201";
				clickOnOK();
			}
			if (checkMaskeID(4202) || checkMaskeID(4226)) {
				if (getMenge().equals("1")) {
					// inputText(getMenge());
					maske = "4226";
					clickOnOK();
				} else {
					if(!checkMaskeID(4202)) {
					inputText(getMenge()); 
				//	Mengeneingabe Teilmenge
				//	inputText("1");
					maske = "4202";
					clickOnOK();
					}
				}
			}

			if (!(checkMaskeID(4216) || checkMaskeID(4010) || checkMaskeID(4080)|| checkMaskeID(7303))) {
// TODO 			
				addValue(getKommGestellfach());
				neuerBox(index);

				gestellfach = getKommGestellfach();

				inputText(getKommGestellfach());
				maske = "4216";
				clickOnOK();

			}
			if (checkMaskeID(4215)) {
				String bdtQuellPlatz = bedienthekeQuellPlatzLokator.getText();
				bdtQuellPlatz = getKommZone() + bdtQuellPlatz.replace("-", "");
				
				String lhmNUmmer = conToDB.getLHMNrvomQuellPlatzVonDB(bdtQuellPlatz,tourBezLokator.getText(), ladefolgeLokator.getText(), standort);
				System.out.println("Das ist die LHM-Nummer vom Platz: " + lhmNUmmer);
				inputText(lhmNUmmer);
				maske = "4215";
				clickOnOK();
			}

			if (checkMaskeID(4221)) {
				inputText(getKommGestellfach());
				maske = "4211";
				clickOnOK();

			}

			// Break, wenn die Bildschirme an einer Maske hängen
			nextMaske = getMaskeID();
			if (maske.equals(nextMaske) && index == 5 && !kommZone.equals("07")) {
				index++;

				// break;

			}
			
			// Anpassung zur Kommisionierung der TRO_C Boxen
			if(checkMaskeID(4267)) {
				String str1 = PIDLokator.getText();
				inputText(str1);
				clickOnOK();
			}
			if ( checkMaskeID(4268)) {
				inputText(getKommGestellfach());
				maske = "4268";
				clickOnOK();

			}
			
			// TODO Hier muss noch die neue Methode getestet werden. 
			scanneFreienBox();

			
		}

		// eventuell Supermarkt scannen
		scanneSupermarkt();
	}

	public void neuerBox(long index) {
		if(!getKommZone().equals("07")) {
		long l = getCountOfValues(getKommGestellfach());

		if (checkMaskeID(4203) && index == l - 1) {
			buttonNeuerBoxLokator.click();
			if (checkMaskeID(4035)) {
				clickOnJa();
			}
			if (checkMaskeID(4202) || checkMaskeID(4226)) {
				if (getMenge().equals("1")) {
					// inputText(getMenge());
					maske = getMaskeID();
					clickOnOK();
				} else {
					// inputText(getMenge());
					inputText("2");
					maske = getMaskeID();
					clickOnOK();
				}
			}

		}
		}
	}

	public void clickOnJa() {
		if (checkMaskeID(4035)) {
		click(buttonJaButtonLokator, 5);
		}
		click(buttonJaSubmitLokator, 5);	 
	 
	}

	public String getVersandart(String mdeNr) {
		if (this.versandart !=null ) {
			return this.versandart;}
		else {
			this.versandart=conToDB.getVersandartVonDB(mdeNr, "BER");
			return this.versandart;}
	}
	
	
	public void scanneSupermarkt() {
		if (checkMaskeID(4205)) {
			String supermarkt = supermarktLocator.getText();
			supermarkt = supermarkt.replace("-", "");
			inputText(supermarkt);
			clickOnOK();
		}
	}
	// check Texthinweis
	public boolean checkTexthinweis(String text) {

	if (hinweisLocator.getText().contains(text)) {  return true;}
	else return false;

	}
	public void setDruckerID(String id) {
		if (checkMaskeID(4204) || checkMaskeID(4081) ) {
			// Input Drucker ID
			inputText(id);
			// Bestätige mit OK
			clickOnOK();
		}
		

	}
	
	
	  public void scanneBeliebegeBoxNrvomGestell() { 
	
		  while(checkMaskeID(4080) )  {
		  if((checkTexthinweis("Beliebiges Etikett scannen") ||
	  checkTexthinweis("Alle Etiketten sind jetzt aufgeklebt."))) {
		String gestell_tmp=  conToDB.getGestellNrVonDB(getMDENR(), "BER");
				if(gestell_tmp.substring(0, 1).equals("2")) {
					gestell_tmp=gestell_tmp.replaceFirst("2", "6");
					}
	  inputText(conToDB.getBeliebegeBoxNrvomGestellFachVonDB(getMDENR(), gestell_tmp , "BER")); 
	  clickOnOK(); } 
		  
	  if(checkMaskeID(4080) && checkTexthinweis("Gestellfach")) {
		  String tmp=	hinweisLocator.getText();
			String str[];
			str = tmp.split(" ");
			tmp=str[1].toString().replace("-", "");
			String gestell_tmp= conToDB.getGestellNrVonDB(getMDENR(), "BER");
			if(gestell_tmp.substring(0, 1).equals("2")) {
				gestell_tmp=gestell_tmp.replaceFirst("2", "6");
				}
			tmp = gestell_tmp + "0"+tmp;
			inputText(tmp);
			clickOnOK();
			
			
	  }
	  
		  }
	  
	  }
	 
	

	public void setLeerenKommGestell() {
		gestell = conToDB.getLeerKommGestellVonDB(standort);
		for(int i=0; i<10 && gestell.equals("Kein Komm-Gestell"); i++) { 
			gestell = conToDB.getLeerKommGestellVonDB(standort);
		}
		if (standort.contentEquals("BER")) {
			if (checkMaskeID(4200) && !(kommZone.equals("08"))) {
				inputText(gestell);
			}
		} else {

			if (checkMaskeID(4200) && !(kommZone.equals("06"))) {
				inputText(gestell);
			}
		}

		/*
		 * if (checkMaskeID(4215) ) { inputText(gestell);
		 * 
		 * 
		 * }
		 */
	}

	public void clickAufWeiter() throws Throwable {
		Thread.sleep(3000);
		
		if (checkMaskeID(4109)) {
			click(weiterButtonLocator, 5);
		}

	}

	public void gewichtEingeben() {
		if (checkMaskeID(4214)) {
			String gewicht = gewichtLokator.getText();
			String[] tmp = gewicht.split(" ");
			String tmp1 = tmp[3].toString() + "1";
			inputText(tmp1);
			clickOnOK();
			clickFertig();

		}
	}
	
	//Konsolidierung im TRO_C - Vorkommissionierung 
	
	public void scanneFreienBox() {
	
			if (checkMaskeID(4080)){
		
			
			if (checkTexthinweis("Freie Box scannen")  ) {
				// TODO   Box AA07 scannen
				String tmp=	hinweisLocator.getText();
				String box=conToDB.getFreienBoxVonDB(getVersandart(getMDENR()),getKommZone(), standort);
				inputText(box);
				clickOnOK();
				
				// inputText(box); clickOnOK();
				
				if (checkMaskeID(4010)) {
					inputText("55");
					//maske = "4010";
					clickOnOK();
				}
				
					}
			
			if (checkMaskeID(4080)){
			String tmp=	hinweisLocator.getText();
			tmp=	hinweisLocator.getText();
			String str[];
			str = tmp.split(" ");
			tmp=str[1].toString();
			inputText(tmp);
			clickOnOK();
			
			if (checkMaskeID(4010)) {
				inputText("55");
				//maske = "4010";
				clickOnOK();
			}
			
			 if  (checkMaskeID(4080) && checkTexthinweis("korrekt")) {
				// TODO   Box AA07 scannen
					tmp=	hinweisLocator.getText();
					tmp=str[1].toString();
					inputText(tmp);
					clickOnOK();

					}
			}
		}		
	}


	  

}
