package helper;


import org.openqa.selenium.WebDriver;

import junit.framework.Assert;

public class MyAssert {
	WebDriver driver;
	public MyAssert(WebDriver driver){
		this.driver = driver;
	}
	
	public void assertTrue(boolean velueToAssert, String reportingMessage){
		try{
			Assert.assertTrue(velueToAssert);
			Report.fetchTest().pass("AssertTrue for  "+reportingMessage + " passed. ", Screenshot.getScreenshot(driver));
		}catch (AssertionError ae){
			Report.fetchTest().fail("AssertTrue failed while "+reportingMessage, Screenshot.getScreenshot(driver));
			throw new AssertionError(ae);
		}
	}
	
}

