package de.bring.pl_mobil;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Base64;
import java.util.Random;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
 

public class ConnectToDB {
private String gestell;
//private List <String> gestellListe;
private String PID;
private String box;
private String aktuellerQ_Platz;

private String tourenGestell;
private String db_Username;
private String db_Passwort;



public static void main(String[] args) {
	ConnectToDB test =new ConnectToDB();
//	System.out.println("KG Test "+test.getLeerKommGestellVonDB("MUC"));
//	System.out.println("KG belegte  "+ test.getBelegteKommGestellVonDB("BER"));  
	System.out.println("Test PID "+ test.getVersandartVonDB( "77", "BER"));
//	System.out.println("Arbeitsbereich "+ test.getAktuellerQ_PlatzVonDB("1204", "BER"));
	
	
	//System.out.println("How to encrypt Data:"+encrypt(readProperty("database.user_MUC.name")));
}
//readProperty("");

public static int getRandom(int from, int to) {
    if (from < to)
        return from + new Random().nextInt(Math.abs(to - from));
    return from - new Random().nextInt(Math.abs(to - from));
}


public void updateProperty(String config, String value) throws ConfigurationException {

	PropertiesConfiguration conf;
	
	conf = new PropertiesConfiguration(
			System.getProperty("user.dir") + "\\src\\main\\resources\\application.properties");
	conf.setProperty(config, value);
	conf.save();

}

public static String decrypt(String encryptedPassword) {
	String decryptedPassword;
	byte[] decryptedPasswordBytes = Base64.getDecoder().decode(encryptedPassword);
	decryptedPassword = new String(decryptedPasswordBytes);
	return decryptedPassword;
	}
public static String encrypt(String password) {
	String encryptedPassword;
	byte[] encodedBytes = Base64.getEncoder().encode(password.getBytes());
	encryptedPassword = new String(encodedBytes);
	return encryptedPassword;
	}

public static String readProperty(String str) {
	String prop;
	PropertiesConfiguration conf;

	try {
		conf = new PropertiesConfiguration(
				System.getProperty("user.dir") + "\\src\\main\\resources\\application.properties");
		prop = conf.getProperty(str).toString();
		conf.save();
		return prop;
	} catch (ConfigurationException e) {
		e.printStackTrace();
		return "leer";
	}
}

// TODO Auto-generated catch block

	public String getLeerKommGestellVonDB(String local){
	try{  //readProperty("URL_BER"),
		String  query;
		String host = decrypt(readProperty("database.host"));
		String port = decrypt(readProperty("database.port"));
		String db_Name = decrypt(readProperty("database.name"));
		if(local.contains("BER")) {
			 db_Username= decrypt(readProperty("database.user_BER.name"));
			 db_Passwort= decrypt(readProperty("database.user_BER.password"));
		}if(local.contains("MUC")) {
				 db_Username= decrypt(readProperty("database.user_MUC.name"));
				 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
			}
		//step1 load the driver class  
		Class.forName(  "oracle.jdbc.driver.OracleDriver");  

		//step2 create  the connection object  
		
		java.sql.Connection con= DriverManager.getConnection(  
		"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

		//step3 create the statement object  
		Statement stmt=con.createStatement();  

		query="SELECT  * FROM (SELECT  gk.GESTELLNR \r\n" + 
				"                FROM GESTELL_POS gp JOIN GESTELL_KOPF gk ON gp.GESTELLNR= gk.GESTELLNR\r\n" + 
				"				WHERE gk.TYP='KG' AND gk.GESTELLNR NOT IN (\r\n" + 
				"				                                            SELECT k.GESTELLNR  FROM GESTELL_KOPF k \r\n" + 
				"				                                            WHERE k.AKT_PLATZ  LIKE '001BC%' OR k.AKT_PLATZ  LIKE '0010599%'\r\n" + 
				"                                                            OR k.AKT_PLATZ  LIKE '0010299%' OR k.AKT_PLATZ  LIKE '0010288%' )\r\n" + 
				"order by dbms_random.value )  WHERE ROWNUM=1";
		
		//step4 execute query  
			
		ResultSet rs=stmt.executeQuery(query);  

		while(rs.next())  
			gestell=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)

			/*
			 * if( gestell==null) { rs=stmt.executeQuery(query); gestell=rs.getString(1);
			 * 
			 * }
			 */
		//step5 close the connection object  
		con.close();  
	/*	tBase = new TestBase();
		tBase.updateProperty("aktuelleKommGestell", gestell);*/
		return gestell;

		}catch(Exception e){ System.out.println(e);}  
			return "Kein Komm-Gestell";
		}  
	// 
	
	// Diese Methode gibt die belegten Komm-Gestelle zurück
	public String getBelegteKommGestellVonDB(String local){
		try{  //readProperty("URL_BER"),
			String  query;
			String host = decrypt(readProperty("database.host"));
			String port = decrypt(readProperty("database.port"));
			String db_Name = decrypt(readProperty("database.name"));
			if(local.contains("BER")) {
				 db_Username= decrypt(readProperty("database.user_BER.name"));
				 db_Passwort= decrypt(readProperty("database.user_BER.password"));
			}if(local.contains("MUC")) {
					 db_Username= decrypt(readProperty("database.user_MUC.name"));
					 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
				}
			//step1 load the driver class  
			Class.forName(  "oracle.jdbc.driver.OracleDriver");  

			//step2 create  the connection object  
			
			java.sql.Connection con= DriverManager.getConnection(  
			"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

			//step3 create the statement object  
			Statement stmt=con.createStatement();  

			query="SELECT  * FROM (   SELECT DISTINCT gk.GESTELLNR \r\n" + 
					"									FROM GESTELL_POS gp \r\n" + 
					"                                    JOIN GESTELL_KOPF gk ON gp.GESTELLNR= gk.GESTELLNR\r\n" + 
					"                                    JOIN BESTAND b ON b.PLATZ=gp.PLATZ\r\n" + 
					"									WHERE gk.TYP='KG' AND b.PALNR >0 AND gk.AKT_PLATZ NOT LIKE '001BC%' \r\n" + 
					"				                   order by dbms_random.value )  WHERE ROWNUM=1";
			
			//step4 execute query  
				
			ResultSet rs=stmt.executeQuery(query);  

			while(rs.next())  
				gestell=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)

				/*
				 * if( gestell==null) { rs=stmt.executeQuery(query); gestell=rs.getString(1);
				 * 
				 * }
				 */
			//step5 close the connection object  
			con.close();  
		/*	tBase = new TestBase();
			tBase.updateProperty("aktuelleKommGestell", gestell);*/
			return gestell;

			}catch(Exception e){ System.out.println(e);}  
				return "Kein Komm-Gestell";
			}  
		
	
	
	
	public String getLeerTourenGestellVonDB(String local){
		try{  
			String host = decrypt(readProperty("database.host"));
			String port = decrypt(readProperty("database.port"));
			String db_Name = decrypt(readProperty("database.name"));
			if(local.contains("BER")) {
				 db_Username= decrypt(readProperty("database.user_BER.name"));
				 db_Passwort= decrypt(readProperty("database.user_BER.password"));
			}else
				{
					 db_Username= decrypt(readProperty("database.user_MUC.name"));
					 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
				}	
			
			
			
			//step1 load the driver class  
			Class.forName("oracle.jdbc.driver.OracleDriver");  

			//step2 create  the connection object  
			java.sql.Connection con= DriverManager.getConnection(  
					"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);    

			//step3 create the statement object  
			Statement stmt=con.createStatement();  
			//getRandom(1001, 9999);
			//step4 execute query  
			ResultSet rs=stmt.executeQuery("SELECT  * FROM (  SELECT gp1.GESTELLNR FROM GESTELL_POS gp1 \r\n" + 
					"					JOIN GESTELL_KOPF gk1 ON gp1.GESTELLNR=gk1.GESTELLNR \r\n" + 
					"					WHERE gk1.TYP='TG' AND gp1.GESTELLNR NOT IN ( SELECT gp.GESTELLNR\r\n" + 
					"					FROM GESTELL_KOPF k  \r\n" + 
					"					JOIN GESTELL_POS gp ON gp.GESTELLNR= k.GESTELLNR \r\n" + 
					"					JOIN PLATZ p ON p.PLATZ_ID=gp.PLATZ\r\n" + 
					"					WHERE p.IST_LHM != '0' OR gp.PLATZ IS NULL \r\n" + 
					"					OR k.AKT_PLATZ  LIKE '001TR%' OR k.AKT_PLATZ  LIKE '001MO%' AND k.MDENR is not null ) order by dbms_random.value )WHERE  ROWNUM=1");  
			while(rs.next())  
				gestell=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)

			//step5 close the connection object  
			con.close();  
			return gestell;

			}catch(Exception e){ System.out.println(e);}  
				return "Keine";
			}  
	
	
	
	

	public String getBereitsBelegtenTourenGestellVonDB(String kommGestell, String tourBez, String local){
		try{  
			System.out.println("Komm-Gestell ist "+kommGestell);
			
			String host = decrypt(readProperty("database.host"));
			String port = decrypt(readProperty("database.port"));
			String db_Name = decrypt(readProperty("database.name"));

			if(local.contains("BER")) {
				 db_Username= decrypt(readProperty("database.user_BER.name"));
				 db_Passwort= decrypt(readProperty("database.user_BER.password"));
			}else
				{
					 db_Username= decrypt(readProperty("database.user_MUC.name"));
					 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
				}			
			
			//step1 load the driver class  
			Class.forName("oracle.jdbc.driver.OracleDriver");  

			//step2 create  the connection object  
			
			java.sql.Connection con= DriverManager.getConnection(  
					"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

			//step3 create the statement object  
			Statement stmt=con.createStatement();  
			//gestellListe =  new ArrayList<String>();
			//step4 execute query  
			ResultSet rs=stmt.executeQuery(" SELECT DISTINCT vgp.GESTELLNR \r\n" + 
					" FROM V_GESTELL_POS vgp \r\n" + 
					" LEFT JOIN GESTELL_POS gp\r\n" + 
					" ON vgp.PLATZ=gp.PLATZ \r\n" + 
					" INNER JOIN GESTELL_KOPF gk\r\n" + 
					" ON gk.GESTELLNR=gp.GESTELLNR AND vgp.GESTELLNR=gk.GESTELLNR\r\n" + 
					" WHERE gk.TYP = 'TG' AND gk1.MDENR is null AND vgp.TOURNR IN (SELECT DISTINCT vgp1.TOURNR FROM V_GESTELL_POS vgp1 WHERE vgp1.Gestellnr = '"+kommGestell+"' AND vgp1.TOURBEZ = '"+tourBez+"')");  
			if(rs.next()){  
				//int i =0;
				 tourenGestell = rs.getString(1); //+","+rs.getString(2)+","+rs.getString(3)+","+rs.getString(3);
				
			}
			
			    System.out.println("Komm-Gestelle "+kommGestell+", Tourbezeichung: "+tourBez);
				System.out.println("Tour-Gestelle "+tourenGestell);
			
			//step5 close the connection object  
			con.close();  
			return tourenGestell;

			}catch(Exception e){ System.out.println(e);}  
				return null;
			}  

	// Diese Methode gibt anhand des Komm-Gestellfaches der PID zurück 
	public String getPIDvomKommGestellVonDB(String gestellfach, String local){
		try{  
			System.out.println("Das ist das Gestellfach: "+gestellfach);
			String host = decrypt(readProperty("database.host"));
			String port = decrypt(readProperty("database.port"));
			String db_Name = decrypt(readProperty("database.name"));
			if(local.contains("BER")) {
			 db_Username= decrypt(readProperty("database.user_BER.name"));
			 db_Passwort= decrypt(readProperty("database.user_BER.password"));
			}else
			{
				 db_Username= decrypt(readProperty("database.user_MUC.name"));
				 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
				}
			//step1 load the driver class  
			Class.forName("oracle.jdbc.driver.OracleDriver");  

			//step2 create  the connection object  
			java.sql.Connection con= DriverManager.getConnection(  
					"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

			//step3 create the statement object  
			Statement stmt=con.createStatement();  

			//step4 execute query  
			ResultSet rs=stmt.executeQuery("SELECT b.LHMNR FROM BESTAND b WHERE b.PLATZ='001"+gestellfach+"' AND ROWNUM=1");  
			while(rs.next())  
				this.PID=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)

			//step5 close the connection object  
			con.close();  
			return this.PID;

			}catch(Exception e){ System.out.println(e);}  
				return null;
			}  
	
	// Diese Methode gibt anhand des Quell-Platzes die LHMNr zurück 
	public String getLHMNrvomQuellPlatzVonDB(String quellPlatz, String tourBez, String ladefolge, String local){
		try{  
			String host = decrypt(readProperty("database.host"));
			String port = decrypt(readProperty("database.port"));
			String db_Name = decrypt(readProperty("database.name"));
			if(local.contains("BER")) {
				 db_Username= decrypt(readProperty("database.user_BER.name"));
				 db_Passwort= decrypt(readProperty("database.user_BER.password"));
			}else
				{
					 db_Username= decrypt(readProperty("database.user_MUC.name"));
					 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
				}
			//step1 load the driver class  
			Class.forName("oracle.jdbc.driver.OracleDriver");  

			//step2 create  the connection object  
			java.sql.Connection con= DriverManager.getConnection(  
					"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

			//step3 create the statement object  
			Statement stmt=con.createStatement();  

			//step4 execute query  
			String query = "SELECT  b.LHMNR FROM BESTAND b JOIN WA_KOPF k ON k.AUFTRAGSNR=b.KD_AUFTRAGSNR JOIN TOURKOPF t ON t.TOURNR=k.TOUR\r\n" + 
					"WHERE b.PLATZ='001"+quellPlatz+"' AND t.BEZ = '"+tourBez+"' AND k.LADEFOLGE ='"+ladefolge+"' AND ROWNUM=1";
					// "SELECT b.LHMNR FROM BESTAND b WHERE b.PLATZ='001"+quellPlatz+"' AND ROWNUM=1"
			ResultSet rs=stmt.executeQuery(query);  
			while(rs.next())  
				this.PID=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)

			//step5 close the connection object  
			con.close();  
			return this.PID;

			}catch(Exception e){ System.out.println(e);}  
				return null;
			} 
	
	// Diese Methode gibt anhand des Quell-Platzes die LHMNr zurück 
		public String getLHMNrvomQuellPlatzVonDB(String quellPlatz, String mdeNr,  String local){
			try{  
				String host = decrypt(readProperty("database.host"));
				String port = decrypt(readProperty("database.port"));
				String db_Name = decrypt(readProperty("database.name"));
				if(local.contains("BER")) {
					 db_Username= decrypt(readProperty("database.user_BER.name"));
					 db_Passwort= decrypt(readProperty("database.user_BER.password"));
				}else
					{
						 db_Username= decrypt(readProperty("database.user_MUC.name"));
						 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
					}
				//step1 load the driver class  
				Class.forName("oracle.jdbc.driver.OracleDriver");  

				//step2 create  the connection object  
				java.sql.Connection con= DriverManager.getConnection(  
						"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

				//step3 create the statement object  
				Statement stmt=con.createStatement();  

				//step4 execute query  
				ResultSet rs=stmt.executeQuery("SELECT b.LHMNR FROM BESTAND b WHERE b.PLATZ='00105"+quellPlatz+"' AND ROWNUM=1"); 
				
				
				
				while(rs.next())  
					this.PID=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)

				//step5 close the connection object  
				con.close();  
				return this.PID;

				}catch(Exception e){ System.out.println(e);}  
					return null;
				}
	
	// Diese Methode gibt anhand des Komm-Gestellfaches der PID zurück 
	public String getPIDvomTourenGestellVonDB(String gestellfach, String local){
		try{  
			System.out.println("Das ist das Gestellfach: "+gestellfach);
			String host = decrypt(readProperty("database.host"));
			String port = decrypt(readProperty("database.port"));
			String db_Name = decrypt(readProperty("database.name"));
			if(local.contains("BER")) {
			 db_Username= decrypt(readProperty("database.user_BER.name"));
			 db_Passwort= decrypt(readProperty("database.user_BER.password"));
			}else
			{
				 db_Username= decrypt(readProperty("database.user_MUC.name"));
				 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
				}
			//step1 load the driver class  
			Class.forName("oracle.jdbc.driver.OracleDriver");  

			//step2 create  the connection object  
			java.sql.Connection con= DriverManager.getConnection(  
					"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

			//step3 create the statement object  
			Statement stmt=con.createStatement();  

			//step4 execute query  
			ResultSet rs=stmt.executeQuery("SELECT b.LHMNR FROM BESTAND  b WHERE b.PLATZ='001"+gestellfach+"' AND ROWNUM=1 ORDER BY b.NEU_DATUM DESC, b.NEU_ZEIT DESC");  
			while(rs.next())  
				this.PID=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)

			//step5 close the connection object  
			con.close();  
			return this.PID;

			}catch(Exception e){ System.out.println(e);}  
				return null;
			}  
	
	
	// Diese Methode gibt anhand des Komm-Gestellfaches der PID zurück 
		public String getPIDvomTourenGestellVonDB(String gestellfach, String pid, String local){
			try{  
				System.out.println("Das ist das Gestellfach: "+gestellfach);
				String host = decrypt(readProperty("database.host"));
				String port = decrypt(readProperty("database.port"));
				String db_Name = decrypt(readProperty("database.name"));
				if(local.contains("BER")) {
				 db_Username= decrypt(readProperty("database.user_BER.name"));
				 db_Passwort= decrypt(readProperty("database.user_BER.password"));
				}else
				{
					 db_Username= decrypt(readProperty("database.user_MUC.name"));
					 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
					}
				//step1 load the driver class  
				Class.forName("oracle.jdbc.driver.OracleDriver");  

				//step2 create  the connection object  
				java.sql.Connection con= DriverManager.getConnection(  
						"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

				//step3 create the statement object  
				Statement stmt=con.createStatement();  

				//step4 execute query  
				ResultSet rs=stmt.executeQuery("SELECT b.LHMNR FROM BESTAND  b WHERE b.PLATZ='001"+gestellfach+"' AND b.LHMNR LIKE '%"+pid+"' AND ROWNUM=1 ORDER BY b.NEU_DATUM DESC, b.NEU_ZEIT DESC");  
				while(rs.next())  
					this.PID=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)

				//step5 close the connection object  
				con.close();  
				return this.PID;

				}catch(Exception e){ System.out.println(e);}  
					return null;
				}  
		
	
	public String getAktuellerQ_PlatzVonDB(String mdeNr, String local){
		try{  
			
			String host = decrypt(readProperty("database.host"));
			String port = decrypt(readProperty("database.port"));
			String db_Name = decrypt(readProperty("database.name"));
			if(local.contains("BER")) {
			 db_Username= decrypt(readProperty("database.user_BER.name"));
			 db_Passwort= decrypt(readProperty("database.user_BER.password"));
			}else
			{
				 db_Username= decrypt(readProperty("database.user_MUC.name"));
				 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
				}
			//step1 load the driver class  
			Class.forName("oracle.jdbc.driver.OracleDriver");  

			//step2 create  the connection object  
			java.sql.Connection con= DriverManager.getConnection(  
					"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

			//step3 create the statement object  
			Statement stmt=con.createStatement();  

			//step4 execute query  
			ResultSet rs=stmt.executeQuery("SELECT fp.Q_PLATZ FROM FAHR_POS fp WHERE fp.ZUSTAND <='20' AND fp.MDENR='"+mdeNr+"' AND ROWNUM=1 ORDER BY fp.ZUSTAND DESC");  
			while(rs.next())  
				this.aktuellerQ_Platz=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)

			//step5 close the connection object  
			con.close();  
			return this.aktuellerQ_Platz;

			}catch(Exception e){ System.out.println(e);}  
				return null;
			}  
	
	
	
	public String getAktuellerZ_PlatzVonDB(String mdeNr, String local){
		try{  
			
			String host = decrypt(readProperty("database.host"));
			String port = decrypt(readProperty("database.port"));
			String db_Name = decrypt(readProperty("database.name"));
			if(local.contains("BER")) {
			 db_Username= decrypt(readProperty("database.user_BER.name"));
			 db_Passwort= decrypt(readProperty("database.user_BER.password"));
			}else
			{
				 db_Username= decrypt(readProperty("database.user_MUC.name"));
				 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
				}
			//step1 load the driver class  
			Class.forName("oracle.jdbc.driver.OracleDriver");  

			//step2 create  the connection object  
			java.sql.Connection con= DriverManager.getConnection(  
					"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

			//step3 create the statement object  
			Statement stmt=con.createStatement();  

			//step4 execute query  
			ResultSet rs=stmt.executeQuery("SELECT fp.Z_PLATZ FROM FAHR_POS fp WHERE fp.ZUSTAND <='20' AND fp.MDENR='"+mdeNr+"' AND ROWNUM=1 ORDER BY fp.ZUSTAND DESC");  
			while(rs.next())  
				this.aktuellerQ_Platz=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)

			//step5 close the connection object  
			con.close();  
			return this.aktuellerQ_Platz;

			}catch(Exception e){ System.out.println(e);}  
				return null;
			}  
	
	// Diese Methode gibt die belegten Komm-Gestelle zurück
		public String getWS_SpurVonDB(String kommSpur, String local){
			try{  //readProperty("URL_BER"),
				String  query;
				String host = decrypt(readProperty("database.host"));
				String port = decrypt(readProperty("database.port"));
				String db_Name = decrypt(readProperty("database.name"));
				if(local.contains("BER")) {
					 db_Username= decrypt(readProperty("database.user_BER.name"));
					 db_Passwort= decrypt(readProperty("database.user_BER.password"));
				}if(local.contains("MUC")) {
						 db_Username= decrypt(readProperty("database.user_MUC.name"));
						 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
					}
				//step1 load the driver class  
				Class.forName(  "oracle.jdbc.driver.OracleDriver");  

				//step2 create  the connection object  
				
				java.sql.Connection con= DriverManager.getConnection(  
				"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

				//step3 create the statement object  
				Statement stmt=con.createStatement();  

				query="SELECT t.WASPUR_PLATZ\r\n" + 
						"FROM TOURKOPF t\r\n" + 
						"WHERE  t.ZUSTAND<'60' AND t.KOMSPUR_MOP_PLATZ LIKE '%"+kommSpur+"%'AND ROWNUM=1";
				
				//step4 execute query  
					
				ResultSet rs=stmt.executeQuery(query);  

				while(rs.next())  
					gestell=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)

					/*
					 * if( gestell==null) { rs=stmt.executeQuery(query); gestell=rs.getString(1);
					 * 
					 * }
					 */
				//step5 close the connection object  
				con.close();  
			/*	tBase = new TestBase();
				tBase.updateProperty("aktuelleKommGestell", gestell);*/
				return gestell;

				}catch(Exception e){ System.out.println(e);}  
					return "Keine WS-Spur";
				}  
			
	
		
		
		public String getBeliebegeBoxNrvomGestellFachVonDB( String mdeNr, String gestellNr, String local){
			try{  
				String host = decrypt(readProperty("database.host"));
				String port = decrypt(readProperty("database.port"));
				String db_Name = decrypt(readProperty("database.name"));
				if(local.contains("BER")) {
					 db_Username= decrypt(readProperty("database.user_BER.name"));
					 db_Passwort= decrypt(readProperty("database.user_BER.password"));
				}else
					{
						 db_Username= decrypt(readProperty("database.user_MUC.name"));
						 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
					}
				//step1 load the driver class  
				Class.forName("oracle.jdbc.driver.OracleDriver");  

				//step2 create  the connection object  
				java.sql.Connection con= DriverManager.getConnection(  
						"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

				//step3 create the statement object  
				Statement stmt=con.createStatement();  

				//step4 execute query  
				ResultSet rs=stmt.executeQuery("SELECT b.LHMNR \r\n" + 
						"FROM GESTELL_POS gp\r\n" + 
						"JOIN GESTELL_KOPF g ON g.GESTELLNR=gp.GESTELLNR\r\n" + 
						"JOIN BESTAND b ON gp.PLATZ=b.PLATZ\r\n" + 
						"WHERE  g.AKT_PLATZ LIKE '001BC00"+mdeNr+"%' AND  b.PLATZ LIKE '001"+gestellNr+"%'  AND ROWNUM=1"); 
				

				
				while(rs.next())  
					PID=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)
				System.out.println("PID_Nummer: " +PID);
				//step5 close the connection object  
				con.close();  
				return PID;

				}catch(Exception e){ System.out.println(e);}  
					return null;
				}


		
		
		public  String getFreienBoxVonDB(String versandart, String kommgruppe, String local){
			try{  //readProperty("URL_BER"),
				String  query, query2;
				String host = decrypt(readProperty("database.host"));
				String port = decrypt(readProperty("database.port"));
				String db_Name = decrypt(readProperty("database.name"));
				if(local.contains("BER")) {
					 db_Username= decrypt(readProperty("database.user_BER.name"));
					 db_Passwort= decrypt(readProperty("database.user_BER.password"));
				}if(local.contains("MUC")) {
						 db_Username= decrypt(readProperty("database.user_MUC.name"));
						 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
					}
				//step1 load the driver class  
				Class.forName(  "oracle.jdbc.driver.OracleDriver");  

				//step2 create  the connection object  
				
				java.sql.Connection con= DriverManager.getConnection(  
				"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

				//step3 create the statement object  
				Statement stmt=con.createStatement();  

				query="SELECT  * FROM (SELECT box.PPID FROM BOX box \r\n" + 
						"WHERE box.SPERR_KNZ='N' AND box.PPID NOT LIKE 'G%' \r\n" + 
						"AND box.PPID NOT IN (SELECT b.LHMNR FROM BESTAND b) order by dbms_random.value ) WHERE ROWNUM=1 ";
				
				
				query2="SELECT  * FROM (SELECT box.PPID FROM BOX box \r\n" + 
						"WHERE box.SPERR_KNZ='N'  AND box.PPID LIKE 'G%'  \r\n" + 
						"AND box.PPID NOT IN (SELECT b.LHMNR FROM BESTAND b) order by dbms_random.value ) WHERE ROWNUM=1 ";
				
				
				
				//step4 execute query  
				ResultSet rs;
				if(versandart.equals("BMP") && kommgruppe.equals("06")) {
					 rs=stmt.executeQuery(query2);  
				}
				else {
				 rs=stmt.executeQuery(query);  
				}

				while(rs.next())  
					gestell=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)

					/*
					 * if( gestell==null) { rs=stmt.executeQuery(query); gestell=rs.getString(1);
					 * 
					 * }
					 */
			//step5 close the connection object  
				con.close();  
			/*	tBase = new TestBase();
				tBase.updateProperty("aktuelleKommGestell", gestell);*/
				return gestell;

				}catch(Exception e){ System.out.println(e);}  
					return "Keine freie Box.";
				}  


		
		public  String getGestellNrVonDB(String mdeNr, String local){
			try{  //readProperty("URL_BER"),
				String  query;
				String host = decrypt(readProperty("database.host"));
				String port = decrypt(readProperty("database.port"));
				String db_Name = decrypt(readProperty("database.name"));
				if(local.contains("BER")) {
					 db_Username= decrypt(readProperty("database.user_BER.name"));
					 db_Passwort= decrypt(readProperty("database.user_BER.password"));
				}if(local.contains("MUC")) {
						 db_Username= decrypt(readProperty("database.user_MUC.name"));
						 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
					}
				//step1 load the driver class  
				Class.forName(  "oracle.jdbc.driver.OracleDriver");  

				//step2 create  the connection object  
				
				java.sql.Connection con= DriverManager.getConnection(  
				"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

				//step3 create the statement object  
				Statement stmt=con.createStatement();  

				query="SELECT g.GESTELLNR\r\n" + 
						"FROM GESTELL_KOPF g\r\n" + 
						"WHERE g.AKT_PLATZ LIKE '001BC00"+mdeNr+"%'";
				
				//step4 execute query  
				
				ResultSet rs=stmt.executeQuery(query);  

				while(rs.next())  
					gestell=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)

					/*
					 * if( gestell==null) { rs=stmt.executeQuery(query); gestell=rs.getString(1);
					 * 
					 * }
					 */
			//step5 close the connection object  
				con.close();  
			/*	tBase = new TestBase();
				tBase.updateProperty("aktuelleKommGestell", gestell);*/
				return gestell;

				}catch(Exception e){ System.out.println(e);}  
					return "Keine freie Box.";
				}
		
		
		public  String getVersandartVonDB(String mdeNr, String local){
			try{  //readProperty("URL_BER"),
				String  query;
				String host = decrypt(readProperty("database.host"));
				String port = decrypt(readProperty("database.port"));
				String db_Name = decrypt(readProperty("database.name"));
				if(local.contains("BER")) {
					 db_Username= decrypt(readProperty("database.user_BER.name"));
					 db_Passwort= decrypt(readProperty("database.user_BER.password"));
				}if(local.contains("MUC")) {
						 db_Username= decrypt(readProperty("database.user_MUC.name"));
						 db_Passwort= decrypt(readProperty("database.user_MUC.password"));
					}
				//step1 load the driver class  
				Class.forName(  "oracle.jdbc.driver.OracleDriver");  

				//step2 create  the connection object  
				
				java.sql.Connection con= DriverManager.getConnection(  
				"jdbc:oracle:thin:@"+host+":"+port+":"+db_Name,db_Username,db_Passwort);  

				//step3 create the statement object  
				Statement stmt=con.createStatement();  

				query="SELECT DISTINCT t.VERSANDART\r\n" + 
						"FROM FAHR_POS fp\r\n" + 
						"JOIN TOURKOPF t ON t.TOURNR=fp.TOURNR\r\n" + 
						"WHERE fp.MDENR="+mdeNr+" AND fp.ZUSTAND<60" ; 
						
				
				//step4 execute query  
				
				ResultSet rs=stmt.executeQuery(query);  

				while(rs.next())  
					gestell=rs.getString(1);  //+"  "+rs.getString(2)+"  "+rs.getString(3)

					/*
					 * if( gestell==null) { rs=stmt.executeQuery(query); gestell=rs.getString(1);
					 * 
					 * }
					 */
			//step5 close the connection object  
				con.close();  
			/*	tBase = new TestBase();
				tBase.updateProperty("aktuelleKommGestell", gestell);*/
				return gestell;

				}catch(Exception e){ System.out.println(e);}  
					return "Keine freie Box.";
				}
		
}
		
	
	

