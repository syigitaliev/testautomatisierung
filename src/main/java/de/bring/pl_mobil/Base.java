package de.bring.pl_mobil;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.http.util.Asserts;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.Keys;
//import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

//import com.sun.tools.javac.util.Assert;

public class Base {
	protected WebDriver driver;
	private static XSSFCell cell;
	// String standort =readDataFromExcel(System.getProperty("user.dir") +
	// "\\src\\main\\resources\\testdata.xlsx","Kommissionierung","Standort" );
//	@FindBy(how = How.CLASS_NAME, using = "paramwinzig")
//	private WebElement maske_ID_Locator;
	
	@FindBy(how = How.XPATH, using = "//*[@id=\"header\"]/table/tbody/tr[1]/td[2]")
	private WebElement maske_ID_Locator;
	
	
	
	@FindBy(how = How.CLASS_NAME, using = "head")
	private WebElement headDescription_Locator;

	@FindBy(how = How.XPATH, using = "//*[@id=\"header\"]/table/tbody/tr[1]/td[3]")
	private WebElement mde_ID_Locator;
	List<String> list;	

	public Base(WebDriver driver) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
		list = Stream.of("one", "two", "three").collect(Collectors.toList());
		  	}

	// Methoden

	public void visit(String url) {
		try {
			driver.get(url);
		} catch (WebDriverException wde) {
			// Report.fetchTest().error(wde, Screenshot.getScreenshot(driver));
			System.out.println("ERROR_MESSAGE: WebPage access denied: " + "\n" + wde);
			Asserts.check(false, "ERROR_MESSAGE: WebPage access denied: " + "\n" + wde);

		}
	}

	public boolean checkMaskeID(int ID) {
		return checkMaske(maske_ID_Locator, ID, 6);
	}
	
	public String checkHeadDescription(String text) {
		return headDescription_Locator.getText();
	}

	public String getMaskeID() {
		return maske_ID_Locator.getText();
	}

	public void type(WebElement element, int timeout, String inputText) {
		try {
			new WebDriverWait(driver, timeout).until(ExpectedConditions.elementToBeClickable(element));
			element.clear();
			element.sendKeys(inputText);
		} catch (WebDriverException wde) {
			System.out.println("ERROR_MESSAGE: In the WebElement is not typeble: " + "\n" + wde);
			Asserts.check(false, "ERROR_MESSAGE: In the WebElement is not typeble: " + "\n" + wde);
		}

	}

	public void click(WebElement element, int timeout) {
		try {
			new WebDriverWait(driver, timeout).until(ExpectedConditions.elementToBeClickable(element)).sendKeys(Keys.RETURN);
		} catch (WebDriverException wde) {
			// Report.fetchTest().error(wde, Screenshot.getScreenshot(driver));
			System.out.println("ERROR_MESSAGE: WebElement is not clickable: " + "\n" + wde);
			Asserts.check(false, "ERROR_MESSAGE: WebElement is not clickable: " + "\n" + wde);
			// Assert.fail();
		}
	}

	public void waitForElementToBeVisible(WebElement element, int timeout) {
		try {
			new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOf(element));
		} catch (WebDriverException wde) {
			System.out.println("ERROR_MESSAGE: WebElement is not visible: " + "\n" + wde);
			Asserts.check(false, "ERROR_MESSAGE: WebElement is not visible: " + "\n" + wde);
		}
	}

	public void select(WebElement element, int timeout, String visibleText) {
		waitForElementToBeVisible(element, timeout);
		Select select = new Select(element);
		try {
			select.selectByVisibleText(visibleText);
		} catch (WebDriverException wde) {
			System.out.println("ERROR_MESSAGE: WebElement is not selectable: " + "\n" + wde);
			Asserts.check(false, "ERROR_MESSAGE: WebElement is not selectable: " + "\n" + wde);
		}
	}

	public boolean checkMaske(WebElement element, int id, int timeout) {
		try {
			new WebDriverWait(driver, timeout).until(ExpectedConditions.visibilityOf(element));
			String maske = element.getText();
			String tmp = "Maske: " + id;
			if (maske.equals(tmp)) {
				return true;
			} else {
				return false;
			}
		} catch (WebDriverException wde) {
			System.out.println("ERROR_MESSAGE: Maske ist nicht sichtbar oder falsch: " + "\n" + wde);
			Asserts.check(false, "ERROR_MESSAGE: Maske ist nicht sichtbar oder falsch: " + "\n" + wde);
			return false;
		}
	}

	//
	/*
	 * public String getMDE_Art(WebElement element, int timeout) { try { new
	 * WebDriverWait(driver,
	 * timeout).until(ExpectedConditions.visibilityOf(element)); String tmp=
	 * element.getText(); String [] id =tmp.split(" "); int mde_ID =
	 * Integer.parseInt(id[1]); System.out.println(mde_ID);
	 * 
	 * if (mde_ID>=800) { return "Waage-"+mde_ID; } else { return "MDE-"+mde_ID; } }
	 * catch (WebDriverException wde) {
	 * System.out.println("ERROR_MESSAGE: Maske ist nicht sichtbar oder falsch: " +
	 * "\n" + wde); // Assert.fail(); return
	 * "Gerättyp konnte nicht ermittelt werden."; } }
	 * 
	 * 
	 * public boolean istWaage() { String tmp=getMDE_Art(mde_ID_Locator, 3);
	 * if(tmp.contains("Waage")) { return true;} else return false; }
	 * 
	 */

	public String decodePassword(String decodedpassword) {

		return decodedpassword;
	}

	public static String  readDataFromExcel(String ExcelFile, String sheetName, String value) throws IOException {
		int RowNum=1, ColNum=1;    String  cellData = null;
			try {
				File src = new File(ExcelFile);
		        FileInputStream file = new FileInputStream(src); 
		        
		      //  FileInputStream file = new FileInputStream(new File(ExcelFile));
		        XSSFWorkbook excelWBook = new XSSFWorkbook(file);
		        XSSFSheet excelWSheet = excelWBook.getSheet(sheetName);
		        DataFormatter formatter = new DataFormatter();
			     
	           
	            int totalNoOfRows = excelWSheet.getLastRowNum();
	            
	            System.out.println("Anzahl Zeile: " +totalNoOfRows );
	            
	            int totalNoOfCols =excelWSheet.getRow(0).getLastCellNum();
	            
	            System.out.println("Anzahl Spalten: " +totalNoOfCols);
				
	            // Inizialisiere ein Array für die Daten
				// arrayExcelData = new String[totalNoOfRows][totalNoOfCols];
				
				// Speichere die Daten in den Array
			   // Identifizierung der  
	            if(value== "Standort") {
			    	  RowNum=1; ColNum=1;
					}
	            
	            else if(value== "Einloggen") {
			    	  RowNum=2; ColNum=1;
					}
			  	else if (value== "Auswahl_Kommissionierung" || value== "Auswahl_Konsolidierung") {
			  	  RowNum=3; ColNum=1;
					}
			  	else if (value== "Scanne_Komm_Gestell") {
			    	  RowNum=4; ColNum=1;
			  		}
			  	else if (value== "Starte_Kommissionierung" ||  value== "Starte_Konsolidierung") {
			  	  RowNum=5; ColNum=1;
					}
			  	else if (value== "Scanne_ET_Drucker" || value== "Abschluss") {
			    	  RowNum=6; ColNum=1;
			  		}
			  	else if (value== "Starte_direkt_ Konsolidierung") {
			  	  RowNum=7; ColNum=1;
					}
			  	else if (value== "Abschluss") {
				  	  RowNum=8; ColNum=1;
						}
	            
			  	else if (value== "BoxVoll bei X (X von 1 bis 100)") {
				  	  RowNum=10; ColNum=1;
						}
			  	else System.out.println("Prozess ist in dem Tabelle "+sheetName+ " nicht definiert." );
			            				
			      // Lese den Wert von der Zelle 
						  cell = excelWSheet.getRow(RowNum).getCell(ColNum);
				            //DataFormatter formatter = new DataFormatter();
				              cellData = formatter.formatCellValue(cell);
				           // return cellData;
						
						 System.out.println("Die Zeile "+RowNum+" und die Spalte "+(ColNum)+ " ist "+cellData);
					
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
				e.printStackTrace();
			}
			return cellData;
		}

public static void  updateDataInExcel(String ExcelFile, String sheetName, String value) throws IOException {
		int RowNum=1, ColNum=1;    String  cellData = null;
			try {
				File src = new File(ExcelFile);
		        FileInputStream file = new FileInputStream(src); 
		        
		      //  FileInputStream file = new FileInputStream(new File(ExcelFile));
		        XSSFWorkbook excelWBook = new XSSFWorkbook(file);
		        XSSFSheet excelWSheet = excelWBook.getSheet(sheetName);
		        DataFormatter formatter = new DataFormatter();
			     
	           
	            int totalNoOfRows = excelWSheet.getLastRowNum();
	            
	            System.out.println("Anzahl Zeile: " +totalNoOfRows );
	            
	            int totalNoOfCols =excelWSheet.getRow(0).getLastCellNum();
	            
	            System.out.println("Anzahl Spalten: " +totalNoOfCols);
				
	            // Inizialisiere ein Array für die Daten
				// arrayExcelData = new String[totalNoOfRows][totalNoOfCols];
				
				// Speichere die Daten in den Array
			   // Identifizierung der  
	            
	            
	            
	            
	            XSSFRow Row = excelWSheet.createRow(excelWSheet.getLastRowNum()+ 1);
	            cell = Row.createCell(1);
	            cell.setCellValue(value);
	            
			            				
	/*		      // Lese den Wert von der Zelle 
						  cell = excelWSheet.getRow(RowNum).getCell(ColNum);
				            //DataFormatter formatter = new DataFormatter();
				              cellData = formatter.formatCellValue(cell);
				           // return cellData;
						
						 System.out.println("Die Zeile "+RowNum+" und die Spalte "+(ColNum)+ " ist "+cellData);
				*/	
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
				e.printStackTrace();
			}
			
		
			}



  // Diese Methode gibt die Anzahl von Einträge in der list-Map 
	public Long getCountOfValues(String fach) {

	Map<String, Long> frequencyMap =
			list.stream().collect(Collectors.groupingBy(Function.identity(), 
													Collectors.counting()));

	for (Map.Entry<String, Long> entry : frequencyMap.entrySet()) {
		//System.out.println(entry.getKey() + ": " + entry.getValue());
	//	System.out.println(entry.getKey());
		if(entry.getKey().toString().equals(fach)) {
			System.out.println(entry.getKey() + "  " +entry.getValue());
			
			
			return  entry.getValue();
		}
	}

	return  null;
}
	  // Diese Methode fügt die Einträge in die list-Map ein
	public void addValue(String fach) {

	list.add(fach);
	Map<String, Long> frequencyMap =
			list.stream().collect(Collectors.groupingBy(Function.identity(), 
													Collectors.counting()));

	for (Map.Entry<String, Long> entry : frequencyMap.entrySet()) {
	//	System.out.println(entry.getKey() + ": " + entry.getValue());
	}
}
	
}

